# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../../code_models.rst:2
msgid "Models"
msgstr ""

#: of remapp.models.AccumCassetteBsdProjRadiogDose:1
msgid "Accumulated Cassette-based Projection Radiography Dose TID 10006"
msgstr ""

#: of remapp.models.AccumCassetteBsdProjRadiogDose:7
#: remapp.models.AccumIntegratedProjRadiogDose:7
#: remapp.models.IrradEventXRayDetectorData:6
#: remapp.models.IrradEventXRayMechanicalData:5
#: remapp.models.IrradEventXRaySourceData:4
msgid "From DICOM Part 16 Correction Proposal CP-1077:"
msgstr ""

#: of remapp.models.AccumCassetteBsdProjRadiogDose:4
msgid ""
"This template provides information on Projection Radiography dose values "
"accumulated on Cassette- based systems over one or more irradiation "
"events (typically a study or a performed procedure step) from the same "
"equipment."
msgstr ""

#: of remapp.models.AccumIntegratedProjRadiogDose:1
msgid "Accumulated Integrated Projection Radiography Dose TID 10007"
msgstr ""

#: of remapp.models.AccumIntegratedProjRadiogDose:4
msgid ""
"This template provides information on Projection Radiography dose values "
"accumulated on Integrated systems over one or more irradiation events "
"(typically a study or a performed procedure step) from the same "
"equipment."
msgstr ""

#: of remapp.models.AccumIntegratedProjRadiogDose.convert_gym2_to_cgycm2:1
msgid "Converts Gy.m2 to cGy.cm2 for display in web interface"
msgstr ""

#: of
#: remapp.models.AccumIntegratedProjRadiogDose.total_dap_delta_gym2_to_cgycm2:1
msgid ""
"Converts total DAP over delta days from Gy.m2 to cGy.cm2 for display in "
"web interface"
msgstr ""

#: of remapp.models.AccumMammographyXRayDose:1
msgid "Accumulated Mammography X-Ray Dose TID 10005"
msgstr ""

#: of remapp.models.AccumMammographyXRayDose:7
#: remapp.models.AccumProjXRayDose:7 remapp.models.AccumXRayDose:7
#: remapp.models.CtAccumulatedDoseData:7 remapp.models.CtDoseCheckDetails:5
#: remapp.models.CtIrradiationEventData:4 remapp.models.CtRadiationDose:9
#: remapp.models.DeviceParticipant:6 remapp.models.ObserverContext:5
#: remapp.models.PersonParticipant:7
#: remapp.models.ProjectionXRayRadiationDose:10 remapp.models.ScanningLength:5
msgid "From DICOM Part 16:"
msgstr ""

#: of remapp.models.AccumMammographyXRayDose:4
msgid ""
"This modality specific template provides detailed information on "
"mammography X-Ray dose value accumulations over several irradiation "
"events from the same equipment (typically a study or a performed "
"procedure step)."
msgstr ""

#: of remapp.models.AccumProjXRayDose:1
msgid "Accumulated Fluoroscopy and Acquisition Projection X-Ray Dose TID 10004"
msgstr ""

#: of remapp.models.AccumProjXRayDose:4 remapp.models.AccumXRayDose:4
msgid ""
"This general template provides detailed information on projection X-Ray "
"dose value accumulations over several irradiation events from the same "
"equipment (typically a study or a performed procedure step)."
msgstr ""

#: of remapp.models.AccumProjXRayDose.acq_gym2_to_cgycm2:1
msgid ""
"Converts acquisition DAP total from Gy.m2 to cGy.cm2 for display in web "
"interface"
msgstr ""

#: of remapp.models.AccumProjXRayDose.fluoro_gym2_to_cgycm2:1
msgid ""
"Converts fluoroscopy DAP total from Gy.m2 to cGy.cm2 for display in web "
"interface"
msgstr ""

#: of remapp.models.AccumXRayDose:1
msgid "Accumulated X-Ray Dose TID 10002"
msgstr ""

#: of remapp.models.AdminTaskQuestions:1
msgid "Record if admin tasks have been dealt with"
msgstr ""

#: of remapp.models.Calibration:1
msgid "Table to hold the calibration information"
msgstr ""

#: of remapp.models.Calibration:3
msgid "Container in TID 10002 Accumulated X-ray dose"
msgstr ""

#: of remapp.models.ContextID:1
msgid "Table to hold all the context ID code values and code meanings."
msgstr ""

#: of remapp.models.ContextID:3
msgid ""
"Could be prefilled from the tables in DICOM 3.16, but is actually "
"populated as the codes occur.     This assumes they are used correctly."
msgstr ""

#: of remapp.models.CtAccumulatedDoseData:1
msgid "CT Accumulated Dose Data"
msgstr ""

#: of remapp.models.CtAccumulatedDoseData:4
msgid ""
"This general template provides detailed information on CT X-Ray dose "
"value accumulations over several irradiation events from the same "
"equipment and over the scope of accumulation specified for the report "
"(typically a Study or a Performed Procedure Step)."
msgstr ""

#: of remapp.models.CtDoseCheckDetails:1
msgid "CT Dose Check Details TID 10015"
msgstr ""

#: of remapp.models.CtDoseCheckDetails:4
msgid ""
"This template records details related to the use of the NEMA Dose Check "
"Standard (NEMA XR-25-2010)."
msgstr ""

#: of remapp.models.CtIrradiationEventData:1
msgid "CT Irradiation Event Data TID 10013"
msgstr ""

#: of remapp.models.CtIrradiationEventData:4 remapp.models.IrradEventXRayData:4
msgid ""
"This template conveys the dose and equipment parameters of a single "
"irradiation event."
msgstr ""

#: of remapp.models.CtIrradiationEventData:9
#: remapp.models.IrradEventXRayMechanicalData:10
#: remapp.models.IrradEventXRaySourceData:10
msgid "Additional to the template:"
msgstr ""

#: of remapp.models.CtIrradiationEventData:7
msgid "date_time_started"
msgstr ""

#: of remapp.models.CtIrradiationEventData:8
msgid "series_description"
msgstr ""

#: of remapp.models.CtRadiationDose:1
msgid "CT Radiation Dose TID 10011"
msgstr ""

#: of remapp.models.CtRadiationDose:4
msgid ""
"This template defines a container (the root) with subsidiary content "
"items, each of which corresponds to a single CT X-Ray irradiation event "
"entry. There is a defined recording observer (the system or person "
"responsible for recording the log, generally the system). Accumulated "
"values shall be kept for a whole Study or at least a part of a Study, if "
"the Study is divided in the workflow of the examination, or a performed "
"procedure step. Multiple CT Radiation Dose objects may be created for one"
" Study."
msgstr ""

#: of remapp.models.CtReconstructionAlgorithm:1
msgid "Container in TID 10013 to hold CT reconstruction methods"
msgstr ""

#: of remapp.models.CtXRaySourceParameters:1
msgid "Container in TID 10013 to hold CT x-ray source parameters"
msgstr ""

#: of remapp.models.DeviceParticipant:1
msgid "Device Participant TID 1021"
msgstr ""

#: of remapp.models.DeviceParticipant:4
msgid ""
"This template describes a device participating in an activity as other "
"than an observer or subject. E.g. for a dose report documenting an "
"irradiating procedure, participants include the irradiating device."
msgstr ""

#: of remapp.models.DicomDeleteSettings:1
msgid "Table to store DICOM deletion settings"
msgstr ""

#: of remapp.models.DicomQuery:1
msgid "Table to store DICOM query settings"
msgstr ""

#: of remapp.models.DicomRemoteQR:1
msgid "Table to store DICOM remote QR settings"
msgstr ""

#: of remapp.models.DicomStoreSCP:1
msgid "Table to store DICOM store settings"
msgstr ""

#: of remapp.models.DoseRelatedDistanceMeasurements:1
msgid "Dose Related Distance Measurements Context ID 10008"
msgstr ""

#: of remapp.models.DoseRelatedDistanceMeasurements:3
msgid "Called from TID 10003c"
msgstr ""

#: of remapp.models.Exports:1
msgid "Table to hold the export status and filenames"
msgstr ""

#: of remapp.models.Exposure:1
msgid "In TID 10003b. Code value 113736 (uAs)"
msgstr ""

#: of remapp.models.Exposure.convert_uAs_to_mAs:1
msgid "Converts uAs to mAs for display in web interface"
msgstr ""

#: of remapp.models.GeneralEquipmentModuleAttr:1
msgid "General Equipment Module C.7.5.1"
msgstr ""

#: of remapp.models.GeneralEquipmentModuleAttr:6
msgid "From DICOM Part 3: Information Object Definitions Table C.7-8:"
msgstr ""

#: of remapp.models.GeneralEquipmentModuleAttr:4
msgid ""
"Specifies the Attributes that identify and describe the piece of "
"equipment that produced a Series of Composite Instances."
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr:1
msgid "General Study Module C.7.2.1"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr:3
msgid ""
"Specifies the Attributes that describe and identify the Study performed "
"upon the Patient. From DICOM Part 3: Information Object Definitions Table"
" C.7-3"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr:13
msgid "Additional to the module definition:"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr:8
msgid "performing_physician_name"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr:9
msgid "operator_name"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr:10
msgid "modality_type"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr:11
msgid "procedure_code_value_and_meaning"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr:12
msgid "requested_procedure_code_value_and_meaning"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr.dap_a_cgycm2:1
msgid "Converts DAP A to cGy.cm2 from Gy.m2 for display or export"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr.dap_b_cgycm2:1
msgid "Converts DAP B to cGy.cm2 from Gy.m2 for display or export"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr.dap_delta_weeks_cgycm2:1
msgid "Converts DAP delta weeks to cGy.cm2 from Gy.m2 for display"
msgstr ""

#: of remapp.models.GeneralStudyModuleAttr.dap_total_cgycm2:1
msgid "Converts DAP A+B to cGy.cm2 from Gy.m2 for display or export"
msgstr ""

#: of remapp.models.HighDoseMetricAlertRecipients:1
msgid "Table to store whether users should receive high dose fluoroscopy alerts"
msgstr ""

#: of remapp.models.HighDoseMetricAlertSettings:1
msgid "Table to store high dose fluoroscopy alert settings"
msgstr ""

#: of remapp.models.HomePageAdminSettings:1
msgid "Table to store home page settings"
msgstr ""

#: of remapp.models.ImageViewModifier:1
msgid ""
"Table to hold image view modifiers for the irradiation event x-ray data "
"table"
msgstr ""

#: of remapp.models.ImageViewModifier:7
msgid "From DICOM Part 16 Annex D DICOM controlled Terminology Definitions"
msgstr ""

#: of remapp.models.ImageViewModifier:4
msgid "Code Value 111032"
msgstr ""

#: of remapp.models.ImageViewModifier:5
msgid "Code Meaning Image View Modifier"
msgstr ""

#: of remapp.models.ImageViewModifier:6
msgid "Code Definition Modifier for image view"
msgstr ""

#: of remapp.models.IrradEventXRayData:1
msgid "Irradiation Event X-Ray Data TID 10003"
msgstr ""

#: of remapp.models.IrradEventXRayData:6
msgid "From DICOM part 16:"
msgstr ""

#: of remapp.models.IrradEventXRayDetectorData:1
msgid "Irradiation Event X-Ray Detector Data TID 10003a"
msgstr ""

#: of remapp.models.IrradEventXRayDetectorData:4
msgid ""
"This template contains data which is expected to be available to the "
"X-ray detector or plate reader component of the equipment."
msgstr ""

#: of remapp.models.IrradEventXRayMechanicalData:1
msgid "Irradiation Event X-Ray Mechanical Data TID 10003c"
msgstr ""

#: of remapp.models.IrradEventXRayMechanicalData:4
msgid ""
"This template contains data which is expected to be available to the "
"gantry or mechanical component of the equipment."
msgstr ""

#: of remapp.models.IrradEventXRayMechanicalData:8
msgid "compression_force"
msgstr ""

#: of remapp.models.IrradEventXRayMechanicalData:9
msgid "magnification_factor"
msgstr ""

#: of remapp.models.IrradEventXRaySourceData:1
msgid "Irradiation Event X-Ray Source Data TID 10003b"
msgstr ""

#: of remapp.models.IrradEventXRaySourceData:4
msgid ""
"This template contains data which is expected to be available to the "
"X-ray source component of the equipment."
msgstr ""

#: of remapp.models.IrradEventXRaySourceData:7
msgid "ii_field_size"
msgstr ""

#: of remapp.models.IrradEventXRaySourceData:8
msgid "exposure_control_mode"
msgstr ""

#: of remapp.models.IrradEventXRaySourceData:9
msgid "grid information over and above grid type"
msgstr ""

#: of remapp.models.IrradEventXRaySourceData.convert_gy_to_mgy:1
msgid "Converts Gy to mGy for display in web interface"
msgstr ""

#: of remapp.models.Kvp:1
msgid "In TID 10003b. Code value 113733 (kV)"
msgstr ""

#: of remapp.models.MergeOnDeviceObserverUIDSettings:1
msgid ""
"Table to store setting(s) for autmoatic setting of Display Name and "
"Modality type based on same Device observer UID"
msgstr ""

#: of remapp.models.NotPatientIndicatorsID:1
msgid ""
"Table to record strings that indicate a patient ID is really a test or QA"
" ID"
msgstr ""

#: of remapp.models.NotPatientIndicatorsName:1
msgid ""
"Table to record strings that indicate a patient name is really a test or "
"QA name"
msgstr ""

#: of remapp.models.ObjectUIDsProcessed:1
msgid ""
"Table to hold the SOP Instance UIDs of the objects that have been "
"processed against this study to enable duplicate sorting."
msgstr ""

#: of remapp.models.ObserverContext:1
msgid "Observer Context TID 1002"
msgstr ""

#: of remapp.models.ObserverContext:4
msgid ""
"The observer (person or device) that created the Content Items to which "
"this context applies."
msgstr ""

#: of remapp.models.PKsForSummedRFDoseStudiesInDeltaWeeks:1
msgid ""
"Table to hold foreign keys of all studies that fall within the delta "
"weeks of each RF study."
msgstr ""

#: of remapp.models.PatientIDSettings:1
msgid "Table to store patient ID settings"
msgstr ""

#: of remapp.models.PatientModuleAttr:1
msgid "Patient Module C.7.1.1"
msgstr ""

#: of remapp.models.PatientModuleAttr:8
msgid "From DICOM Part 3: Information Object Definitions Table C.7-1:"
msgstr ""

#: of remapp.models.PatientModuleAttr:4
msgid ""
"Specifies the Attributes of the Patient that describe and identify the "
"Patient who is the subject of a diagnostic Study. This Module contains "
"Attributes of the patient that are needed for diagnostic interpretation "
"of the Image and are common for all studies performed on the patient. It "
"contains Attributes that are also included in the Patient Modules in "
"Section C.2."
msgstr ""

#: of remapp.models.PatientStudyModuleAttr:1
msgid "Patient Study Module C.7.2.2"
msgstr ""

#: of remapp.models.PatientStudyModuleAttr:6
msgid "From DICOM Part 3: Information Object Definitions Table C.7-4a:"
msgstr ""

#: of remapp.models.PatientStudyModuleAttr:4
msgid ""
"Defines Attributes that provide information about the Patient at the time"
" the Study started."
msgstr ""

#: of remapp.models.PersonParticipant:1
msgid "Person Participant TID 1020"
msgstr ""

#: of remapp.models.PersonParticipant:4
msgid ""
"This template describes a person participating in an activity as other "
"than an observer or subject. E.g. for a dose report documenting an "
"irradiating procedure, participants include the person administering the "
"irradiation and the person authorizing the irradiation."
msgstr ""

#: of remapp.models.ProjectionXRayRadiationDose:1
msgid "Projection X-Ray Radiation Dose template TID 10001"
msgstr ""

#: of remapp.models.ProjectionXRayRadiationDose:4
msgid ""
"This template defines a container (the root) with subsidiary content "
"items, each of which represents a single projection X-Ray irradiation "
"event entry or plane-specific dose accumulations. There is a defined "
"recording observer (the system or person responsible for recording the "
"log, generally the system). A Biplane irradiation event will be recorded "
"as two individual events, one for each plane. Accumulated values will be "
"kept separate for each plane."
msgstr ""

#: of remapp.models.PulseWidth:1
msgid "In TID 10003b. Code value 113793 (ms)"
msgstr ""

#: of remapp.models.ScanningLength:1
msgid "Scanning Length TID 10014"
msgstr ""

#: of remapp.models.ScanningLength:4
msgid "No description"
msgstr ""

#: of remapp.models.SizeSpecificDoseEstimation:1
msgid "Container in TID 10013 to hold size specific dose estimation details"
msgstr ""

#: of remapp.models.SizeUpload:1
msgid "Table to store patient size information"
msgstr ""

#: of remapp.models.SkinDoseMapCalcSettings:1
msgid "Table to store skin dose map calculation settings"
msgstr ""

#: of remapp.models.SkinDoseMapResults:1
msgid "Table to hold the results from OpenSkin"
msgstr ""

#: of remapp.models.SourceOfCTDoseInformation:1
msgid "Source of CT Dose Information"
msgstr ""

#: of remapp.models.SummaryFields:1
msgid ""
"Status and progress of populating the summary fields in "
"GeneralStudyModuleAttr"
msgstr ""

#: of remapp.models.UniqueEquipmentNames:1
msgid "Table to unique equipment name information"
msgstr ""

#: of remapp.models.UpgradeStatus:1
msgid "Record upgrade status activity"
msgstr ""

#: of remapp.models.UserProfile:1
msgid "Table to store user profile settings"
msgstr ""

#: of remapp.models.WEDSeriesOrInstances:1
msgid ""
"From TID 10013 Series or Instance used for Water Equivalent Diameter "
"estimation"
msgstr ""

#: of remapp.models.XrayFilters:1
msgid "Container in TID 10003b. Code value 113771"
msgstr ""

#: of remapp.models.XrayGrid:1
msgid "Content ID 10017 X-Ray Grid"
msgstr ""

#: of remapp.models.XrayGrid:3
msgid "From DICOM Part 16"
msgstr ""

#: of remapp.models.XrayTubeCurrent:1
msgid "In TID 10003b. Code value 113734 (mA)"
msgstr ""

#: of remapp.models.create_or_save_high_dose_metric_alert_recipient_setting:1
msgid "Function to create or save fluoroscopy high dose alert recipient settings"
msgstr ""

