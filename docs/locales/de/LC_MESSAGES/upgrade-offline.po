# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-04 12:05+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../../upgrade-offline.rst:3 36efd1de99e44d4683907970fd9c5b52
msgid "Upgrade an offline OpenREM installation"
msgstr ""

#: ../../upgrade-offline.rst:5 661f8e12af9b4785b7eb76e1e8de4bcb
msgid ""
"Upgrading OpenREM requires new Python packages to be available as well as"
" the latest version of OpenREM. These can be downloaded on any computer "
"with Python 2.7 installed and an internet connection, though if you have "
"trouble when installing the packages you might need to use a similar "
"computer to the one you are installing on - same operating system and "
"matching 32-bit or 64-bit."
msgstr ""

#: ../../upgrade-offline.rst:10 61db2dd4977141b1afe2221f024fae6a
msgid ""
"OpenREM version 0.10 has a minimum Python version of 2.7.9. Use the "
"instructions in the :doc:`upgrade_previous_0.10.0` release notes to check"
" this before downloading the new OpenREM packages. The latest Python 2.7 "
"installation can be obtained from "
"https://www.python.org/downloads/windows/"
msgstr ""

#: ../../upgrade-offline.rst:14 2c932ae3f8ce4004af2d9078252ff936
msgid "Follow the link to the 'Latest Python 2 release'"
msgstr ""

#: ../../upgrade-offline.rst:15 9cc68825ed9949e89e19b98f97c7033d
msgid "Download either the ``Windows x86 MSI installer`` for 32-bit Windows or"
msgstr ""

#: ../../upgrade-offline.rst:16 60c2012490844b1aa458f983cfa281ec
msgid "Download ``Windows x86-64 MSI installer`` for 64-bit Windows"
msgstr ""

#: ../../upgrade-offline.rst:19 4d0fe385efe3427d80e1125ddddb037f
msgid "On a computer with internet access"
msgstr ""

#: ../../upgrade-offline.rst:21 9c8e9ecd0f054962ada38ebc87f2f42d
msgid ""
"In a console, navigate to a suitable place and create a new directory to "
"collect all the packages in, then use pip to download them all:"
msgstr ""

#: ../../upgrade-offline.rst:29 9b5054507c984821916c6b9ba441ed46
msgid "Download specific version of Celery:"
msgstr ""

#: ../../upgrade-offline.rst:31 ../../upgrade-offline.rst:79
#: 866b81e75f42458ebd7e95b305c1041c baa41cd0d42e45fdbcaa01b2456d6ec4
msgid "**Linux server:**"
msgstr ""

#: ../../upgrade-offline.rst:37 ../../upgrade-offline.rst:85
#: 04b22784b21a4142bf9bb10a11985ef4 aa6044a324594c8cb1df57a74e7d4e7a
msgid "**Windows server:**"
msgstr ""

#: ../../upgrade-offline.rst:43 4c99815d5e904df3aef90bd443bd1684
msgid "Download OpenREM and all other dependencies:"
msgstr ""

#: ../../upgrade-offline.rst:50 742def9fe67d42f7a59d1fe589ae6e43
msgid "Copy everything to the OpenREM server"
msgstr ""

#: ../../upgrade-offline.rst:52 0ff85b8992884e1a83b2caa7bc0c7ebb
msgid "Copy the directory to the OpenREM server"
msgstr ""

#: ../../upgrade-offline.rst:55 7d655168e71f494fafcfed9ddf95ba03
msgid "On the OpenREM server without internet access"
msgstr ""

#: ../../upgrade-offline.rst:57 9c38a69652dd4faa891f0e033dbd88fa
msgid "Back up your database"
msgstr ""

#: ../../upgrade-offline.rst:59 72ae74a90a754bab979238c24c403664
msgid "For PostgreSQL on linux you can refer to :ref:`backup-psql-db`"
msgstr ""

#: ../../upgrade-offline.rst:60 d7697581eb1241f4910a4c3e0b7cfd58
msgid "For PostgreSQL on Windows you can refer to :doc:`backupRestorePostgreSQL`"
msgstr ""

#: ../../upgrade-offline.rst:61 161b2f3e71e1424e8df1f0df90f1d54c
msgid ""
"For a non-production SQLite3 database, simply make a copy of the database"
" file"
msgstr ""

#: ../../upgrade-offline.rst:63 6f3d898c550e4d05bda438a2956b49c1
msgid "Stop any Celery workers"
msgstr ""

#: ../../upgrade-offline.rst:65 3cf162bd13944f0cae0c851c33bcdef8
msgid ""
"Consider temporarily disabling your DICOM Store SCP, or redirecting the "
"data to be processed later"
msgstr ""

#: ../../upgrade-offline.rst:67 6bbd9957b0724a58b84406970cef329a
msgid "Install the new version of Python 2.7 if applicable"
msgstr ""

#: ../../upgrade-offline.rst:69 c5b2fbdd3b3a4737b531cf60584dac20
msgid "If you are using a virtualenv, activate it"
msgstr ""

#: ../../upgrade-offline.rst:71 b68821431b3f4fd79d3080232b382885
msgid "Upgrade setuptools:"
msgstr ""

#: ../../upgrade-offline.rst:77 09c03b73047b43fab94a0fef5b44077b
msgid "Install specific version of Celery:"
msgstr ""

#: ../../upgrade-offline.rst:91 d1a4e91f63734e9b872dc7706a3a42ca
msgid "Install OpenREM:"
msgstr ""

#: ../../upgrade-offline.rst:97 79888a2f0db948d9b051211746b9bc53
msgid ""
"Now go back to :ref:`update_configuration0100`, migrate the database and "
"finish the upgrade."
msgstr ""

