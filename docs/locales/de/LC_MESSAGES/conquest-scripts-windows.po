# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-04 12:05+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../../conquest-scripts-windows.rst:2 cf4b330832f3453dba9453ba2b0e86e2
msgid "Creating batch scripts on windows"
msgstr ""

#: ../../conquest-scripts-windows.rst:4 167e3344fa9c4adfa8bcca9bd7524f7d
msgid ""
"Create and save a bash script for each of RDSR, mammo, DX and Philips CT "
"dose images, as required. They should have content something like the "
"following."
msgstr ""

#: ../../conquest-scripts-windows.rst:7 81f01805ae9f461386dcba6c08a1b89e
msgid ""
"These scripts assume there is no virtualenv in use. If you are using one,"
" simply refer to the python executable and OpenREM script in your "
"virtualenv rather than the system wide version."
msgstr ""

#: ../../conquest-scripts-windows.rst:10 51890544f8664d61ad3460a2f083390e
msgid "Radiation Dose Structured Reports"
msgstr ""

#: ../../conquest-scripts-windows.rst:18 85cfcdb0992344cc8cc2a2b7d4ca456f
msgid "Mammography images"
msgstr ""

#: ../../conquest-scripts-windows.rst:26 5f8876731ea1446881a4b63c062c47b6
msgid "Radiography images (DX, and CR that might be DX)"
msgstr ""

#: ../../conquest-scripts-windows.rst:34 150b1fe99b024a71b54f5215f19b03d6
msgid "Philips CT dose info images for Philips CT systems with no RDSR"
msgstr ""

