# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-04 12:05+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../../conquestExampleDicomIni.rst:2 630e8ef8262a48b197d9bd939100c6e2
msgid "Example Windows Conquest dicom.ini file"
msgstr ""

#: ../../conquestExampleDicomIni.rst:4 547d41ca9ef7428b8c73862566fefcb5
msgid ""
"Below is an example ``dicom.ini`` file, including comments describing the"
" function of some sections. The file calls various lua scripts - see the "
"Conquest import configuration document for an example - "
":doc:`conquestImportConfig`."
msgstr ""

#: ../../conquestExampleDicomIni.rst:7 98af7ab383dd4256be1f979a33a2f5f6
msgid "The example ``dicom.ini`` file::"
msgstr ""

