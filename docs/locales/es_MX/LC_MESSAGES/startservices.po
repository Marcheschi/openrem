# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../../startservices.rst:3
msgid "Start all the services"
msgstr ""

#: ../../startservices.rst:6
msgid "Test web server"
msgstr ""

#: ../../startservices.rst:8
msgid "In a shell/command window, move into the openrem folder:"
msgstr ""

#: ../../startservices.rst:10 ../../startservices.rst:86
#: ../../startservices.rst:143 ../../startservices.rst:188
msgid "Ubuntu linux: ``/usr/local/lib/python2.7/dist-packages/openrem/``"
msgstr ""

#: ../../startservices.rst:11 ../../startservices.rst:87
#: ../../startservices.rst:144 ../../startservices.rst:189
msgid "Other linux: ``/usr/lib/python2.7/site-packages/openrem/``"
msgstr ""

#: ../../startservices.rst:12 ../../startservices.rst:88
#: ../../startservices.rst:145 ../../startservices.rst:190
msgid ""
"Linux virtualenv: ``vitualenvfolder/lib/python2.7/site-"
"packages/openrem/`` (remember to activate the virtualenv)"
msgstr ""

#: ../../startservices.rst:13 ../../startservices.rst:89
#: ../../startservices.rst:146 ../../startservices.rst:191
msgid "Windows: ``C:\\Python27\\Lib\\site-packages\\openrem\\``"
msgstr ""

#: ../../startservices.rst:14 ../../startservices.rst:90
#: ../../startservices.rst:147 ../../startservices.rst:192
msgid ""
"Windows virtualenv: ``virtualenvfolder\\Lib\\site-packages\\openrem\\`` "
"(remember to activate the virtualenv)"
msgstr ""

#: ../../startservices.rst:17
msgid "Web access on OpenREM server only"
msgstr ""

#: ../../startservices.rst:19
msgid "Run the built in web server:"
msgstr ""

#: ../../startservices.rst:25
msgid ""
"In a web browser on the same computer, go to http://localhost:8000/ - you"
" should now see the message about creating users. For full functionality "
"start the `Celery task queue`_ before moving on to `Configure the "
"settings`_."
msgstr ""

#: ../../startservices.rst:29
msgid "Web access on other computers"
msgstr ""

#: ../../startservices.rst:31
msgid ""
"The built-in webserver only provides a service on the computer OpenREM is"
" installed on by default (it's only there really for testing). To view "
"the OpenREM interface on another computer, you need to modify the "
"``runserver`` command:"
msgstr ""

#: ../../startservices.rst:38
msgid ""
"This will enable the web service to be available from other computers on "
"the network. If your server has several network cards and you want to "
"restrict it to one, then you can use a real address rather than "
"``0.0.0.0``. Likewise you can specify the port (here it is ``8000``)."
msgstr ""

#: ../../startservices.rst:42
msgid ""
"In a web browser on a different computer on the same network, go to "
"http://192.168.1.10:8000/ (**changing the IP address** to the one you are"
" running the server on) and you should see the OpenREM interface and the "
"message about creating users. For full functionality start the `Celery "
"task queue`_ before moving on to `Configure the settings`_."
msgstr ""

#: ../../startservices.rst:48
msgid ""
"Why are we using the ``--insecure`` option? With ``DEBUG`` mode set to "
"``True`` the test web server would serve up the static files. In this "
"release, ``DEBUG`` mode is set to ``False``, which prevents the test web "
"server serving those files. The ``--insecure`` option allows them to be "
"served again."
msgstr ""

#: ../../startservices.rst:56
msgid "Celery task queue"
msgstr ""

#: ../../startservices.rst:58
msgid ""
"Celery will have been automatically installed with OpenREM, and along "
"with RabbitMQ allows for asynchronous task processing for imports, "
"exports and DICOM networking tasks."
msgstr ""

#: ../../startservices.rst:63
msgid ""
"Celery needs to be able to write to the place where the Celery logs and "
"pid file are to be stored, so make sure:"
msgstr ""

#: ../../startservices.rst:65
msgid ""
"the folder exists (the suggestion below is to create a folder in the "
"``MEDIA_ROOT`` location)"
msgstr ""

#: ../../startservices.rst:66
msgid "the user that starts Celery can write to that folder"
msgstr ""

#: ../../startservices.rst:68
msgid ""
"You can put the folder wherever you like, for example you might like to "
"create a ``/var/log/openrem/`` folder on a linux system."
msgstr ""

#: ../../startservices.rst:71
msgid ""
"If you are using the built-in `Test web server`_ then Celery and the "
"webserver will be running as your user. If you are running a production "
"webserver, such as Apache or nginx on linux, then the user that runs "
"those daemons will need to be able to write to the ``MEDIA_ROOT`` and the"
" Celery log files folder. In this case, you need to change the ownership "
"of the folders and change to the right user before running Celery. On "
"Ubuntu:"
msgstr ""

#: ../../startservices.rst:82
msgid "Now start celery..."
msgstr ""

#: ../../startservices.rst:84 ../../startservices.rst:141
msgid "Move into the openrem folder:"
msgstr ""

#: ../../startservices.rst:92 ../../startservices.rst:155
msgid "Linux - ``\\`` is the line continuation character:"
msgstr ""

#: ../../startservices.rst:99
msgid ""
"Windows - ``celery multi`` doesn't work on Windows, and ``^`` is the "
"continuation character:"
msgstr ""

#: ../../startservices.rst:109
msgid "Celery concurrency"
msgstr ""

#: ../../startservices.rst:111
msgid ""
"Set the number of workers (concurrency, ``-c``) according to how many "
"processor cores you have available. The more you have, the more processes"
" (imports, exports, query-retrieve operations etc) can take place "
"simultaneously. However, each extra worker uses extra memory and if you "
"have too many they will be competing for CPU resources too."
msgstr ""

#: ../../startservices.rst:115
msgid "Problems with Celery 4 on Windows"
msgstr ""

#: ../../startservices.rst:117
msgid ""
"Full support for Celery on Windows was dropped with version 4 due to lack"
" of Windows based developers. Therefore for Windows the instructions fix "
"Celery at version ``3.1.25`` to retain full functionality."
msgstr ""

#: ../../startservices.rst:120
msgid "To stop the celery queues in Linux:"
msgstr ""

#: ../../startservices.rst:126
msgid "For Windows, just press ``Ctrl+c``"
msgstr ""

#: ../../startservices.rst:128
msgid ""
"You will need to do this twice if there are running tasks you wish to "
"kill."
msgstr ""

#: ../../startservices.rst:130 ../../startservices.rst:169
msgid "For production use, see `Daemonising Celery`_ below."
msgstr ""

#: ../../startservices.rst:135
msgid "Celery task management: Flower"
msgstr ""

#: ../../startservices.rst:137
msgid ""
"Flower will have been automatically installed with OpenREM and enables "
"monitoring and management of Celery tasks."
msgstr ""

#: ../../startservices.rst:139
msgid ""
"You should start Flower with the same user that you started Celery with, "
"and put the log file in the same place too."
msgstr ""

#: ../../startservices.rst:149
msgid ""
"If you need to change the default port from 5555 then you need to make "
"the same change in ``openremproject\\local_settings.py`` to add/modify "
"the line ``FLOWER_PORT = 5555``"
msgstr ""

#: ../../startservices.rst:152
msgid ""
"If you wish to be able to use the Flower management interface "
"independently of OpenREM, then omit the ``--address`` part of the "
"command. Flower will then be available from any PC on the network at "
"http://yourdoseservernameorIP:5555/"
msgstr ""

#: ../../startservices.rst:162
msgid "Windows - ``^`` is the line continuation character:"
msgstr ""

#: ../../startservices.rst:174
msgid "Celery periodic tasks: beat"
msgstr ""

#: ../../startservices.rst:178
msgid ""
"Celery beat is only required if you are using the :ref:`nativestore`. "
"Please read the warnings there before deciding if you need to run Celery "
"beat. At the current time, using a third party DICOM store service is "
"recommended for most users. See the :ref:`configure_third_party_DICOM` "
"documentation for more details"
msgstr ""

#: ../../startservices.rst:182
msgid ""
"Celery beat is a scheduler. If it is running, then every 60 seconds a "
"task is run to check if any of the DICOM Store SCP nodes are set to "
"``keep_alive``, and if they are, it tries to verify they are running with"
" a DICOM echo. If this is not successful, then the Store SCP is started."
msgstr ""

#: ../../startservices.rst:186
msgid "To run celery beat, open a new shell and move into the openrem folder:"
msgstr ""

#: ../../startservices.rst:194
msgid "Linux::"
msgstr ""

#: ../../startservices.rst:200
msgid "Windows::"
msgstr ""

#: ../../startservices.rst:206
msgid "For production use, see `Daemonising Celery`_ below"
msgstr ""

#: ../../startservices.rst:208
msgid ""
"As with starting the Celery workers, the folder that the pid, log and for"
" beat, schedule files are to be written **must already exist** and the "
"user starting Celery beat must be able write to that folder."
msgstr ""

#: ../../startservices.rst:211
msgid "To stop Celery beat, just press ``Ctrl+c``"
msgstr ""

#: ../../startservices.rst:216
msgid "Configure the settings"
msgstr ""

#: ../../startservices.rst:219
msgid ""
"Follow the link presented on the front page to get to the user and group "
"administration."
msgstr ""

#: ../../startservices.rst:229
msgid ""
"After the first users are configured, this link will no longer be "
"presented and instead you can go to ``Config -> Users``."
msgstr ""

#: ../../startservices.rst:231
msgid ""
"You will need the superuser username and password you created just after "
"creating the database. The groups are"
msgstr ""

#: ../../startservices.rst:233
msgid "``viewgroup`` can browse the data only"
msgstr ""

#: ../../startservices.rst:234
msgid ""
"``importsizegroup`` can use the csv import facility to add patient height"
" and weight information"
msgstr ""

#: ../../startservices.rst:235
msgid ""
"``importqrgroup`` can use the DICOM query-retrieve facility to pull in "
"studies, as long as they are pre-configured"
msgstr ""

#: ../../startservices.rst:236
msgid "``exportgroup`` can view and export data to a spreadsheet"
msgstr ""

#: ../../startservices.rst:237
msgid ""
"``pidgroup`` can search using patient names and IDs depending on "
"settings, and export with patient names and IDs if they are also a member"
" of the ``exportgroup``"
msgstr ""

#: ../../startservices.rst:239
msgid ""
"``admingroup`` can delete studies, configure DICOM Store/QR settings, "
"configure DICOM keep or delete settings, configure patient ID settings, "
"and abort and delete patient size import jobs. *Members of the admingroup"
" no longer inherit the other groups permissions.*"
msgstr ""

#: ../../startservices.rst:247
msgid ""
"In addition to adding users to these groups, you may like to grant a "
"second user ``superuser`` and ``staff`` status so that there are at least"
" two people who can manage the users"
msgstr ""

#: ../../startservices.rst:249
msgid "Return to the OpenREM interface (click on ``View site`` at the top right)"
msgstr ""

#: ../../startservices.rst:255
msgid ""
"Follow the link to see more information about how you want OpenREM to "
"identify non-patient exposures, such as QA. See "
":doc:`i_not_patient_indicator`."
msgstr ""

#: ../../startservices.rst:257
msgid ""
"Go to ``Config -> DICOM object delete settings`` and configure "
"appropriately (see :doc:`i_deletesettings`)"
msgstr ""

#: ../../startservices.rst:258
msgid ""
"Go to ``Config -> Patient ID settings`` and configure appropriately (see "
":doc:`patientid`)"
msgstr ""

#: ../../startservices.rst:259
msgid ""
"If you want to use OpenREM as a DICOM store, or to use OpenREM to query "
"remote systems, go to ``Config -> Dicom network configuration``. For more"
" information go to :doc:`import`."
msgstr ""

#: ../../startservices.rst:261
msgid ""
"With data in the system, you will want to go to ``Config -> View and edit"
" display names`` and customise the display names. An established system "
"will have several entries for each device, from each time the software "
"version, station name or other elements changes. See "
":doc:`i_displaynames` for more information"
msgstr ""

#: ../../startservices.rst:268
msgid "Start using it - add some data!"
msgstr ""

#: ../../startservices.rst:270
msgid "See :doc:`import`"
msgstr ""

#: ../../startservices.rst:274
msgid "Further instructions"
msgstr ""

#: ../../startservices.rst:277
msgid "Daemonising Celery"
msgstr ""

#: ../../startservices.rst:279
msgid ""
"In a production environment, Celery will need to start automatically and "
"not depend on a particular user being logged in. Therefore, much like the"
" webserver, it will need to be daemonised."
msgstr ""

