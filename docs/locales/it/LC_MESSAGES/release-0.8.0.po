# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../../release-0.8.0.rst:3
msgid "OpenREM Release Notes version 0.8.0"
msgstr ""

#: ../../release-0.8.0.rst:7
msgid "Headline changes"
msgstr ""

#: ../../release-0.8.0.rst:9
msgid ""
"This release has extensive automated testing for large parts of the "
"codebase (for the first time)"
msgstr ""

#: ../../release-0.8.0.rst:10
msgid ""
"Code quality is much improved, reduced duplication, better documentation,"
" many bugs fixed"
msgstr ""

#: ../../release-0.8.0.rst:11
msgid "Imports: RDSR from a wider range of systems now import properly"
msgstr ""

#: ../../release-0.8.0.rst:12
msgid ""
"Imports: Better distinction and control over defining RDSR studies as RF "
"or DX"
msgstr ""

#: ../../release-0.8.0.rst:13
msgid ""
"Imports: Code and instructions to generate and import RDSR from older "
"Toshiba CT scanners"
msgstr ""

#: ../../release-0.8.0.rst:14
msgid "Imports: DICOM Query-Retrieve functionality has been overhauled"
msgstr ""

#: ../../release-0.8.0.rst:15
msgid ""
"Imports: Duplicate checking improved to allow cumulative and continued "
"study RDSRs to import properly"
msgstr ""

#: ../../release-0.8.0.rst:16
msgid ""
"Imports: indicators that a study is not a patient can now be configured "
"in the web interface"
msgstr ""

#: ../../release-0.8.0.rst:17
msgid "Imports, display and export: Better handling of non-ASCII characters"
msgstr ""

#: ../../release-0.8.0.rst:18
msgid ""
"Interface: More detailed, consistent and faster rendering of the data in "
"the web interface"
msgstr ""

#: ../../release-0.8.0.rst:19
msgid ""
"Interface: Maps of fluoroscopy radiation exposure incident on a phantom "
"(Siemens RDSRs only)"
msgstr ""

#: ../../release-0.8.0.rst:20
msgid "Interface: More and better charts, including scatter plots for mammography"
msgstr ""

#: ../../release-0.8.0.rst:21
msgid ""
"Interface: Display names dialogue has been extended to allow "
"administration of all studies from each source"
msgstr ""

#: ../../release-0.8.0.rst:22
msgid "Exports: Much faster, and more consistent"
msgstr ""

#: ../../release-0.8.0.rst:23
msgid "Documentation: Extensive user documentation improvements"
msgstr ""

#: ../../release-0.8.0.rst:29
msgid "Upgrading an OpenREM server with no internet access"
msgstr ""

#: ../../release-0.8.0.rst:31
msgid ""
"Follow the instructions found at :doc:`upgrade-offline`, before returning"
" here to update the database and configuration."
msgstr ""

#: ../../release-0.8.0.rst:35
msgid "Upgrading from version 0.7.4 or previous 0.8.0 betas"
msgstr ""

#: ../../release-0.8.0.rst:38
msgid "Upgrade"
msgstr ""

#: ../../release-0.8.0.rst:40
msgid "Back up your database"
msgstr ""

#: ../../release-0.8.0.rst:42
msgid "For PostgreSQL on linux you can refer to :ref:`backup-psql-db`"
msgstr ""

#: ../../release-0.8.0.rst:43
msgid "For PostgreSQL on Windows you can refer to :ref:`backupRestorePostgreSQL`"
msgstr ""

#: ../../release-0.8.0.rst:44
msgid ""
"For a non-production SQLite3 database, simply make a copy of the database"
" file"
msgstr ""

#: ../../release-0.8.0.rst:46
msgid "Stop any Celery workers"
msgstr ""

#: ../../release-0.8.0.rst:48
msgid ""
"Consider temporarily disabling your DICOM StoreSCP, or redirecting the "
"data to be processed later"
msgstr ""

#: ../../release-0.8.0.rst:50
msgid "If you are using a virtualenv, activate it"
msgstr ""

#: ../../release-0.8.0.rst:52
msgid "Install the new version of OpenREM:"
msgstr ""

#: ../../release-0.8.0.rst:59
msgid "Update the configuration"
msgstr ""

#: ../../release-0.8.0.rst:61
msgid "Locate and edit your local_settings file"
msgstr ""

#: ../../release-0.8.0.rst:63
msgid ""
"Ubuntu linux: ``/usr/local/lib/python2.7/dist-"
"packages/openrem/openremproject/local_settings.py``"
msgstr ""

#: ../../release-0.8.0.rst:64
msgid ""
"Other linux: ``/usr/lib/python2.7/site-"
"packages/openrem/openremproject/local_settings.py``"
msgstr ""

#: ../../release-0.8.0.rst:65
msgid ""
"Linux virtualenv: ``vitualenvfolder/lib/python2.7/site-"
"packages/openrem/openremproject/local_settings.py``"
msgstr ""

#: ../../release-0.8.0.rst:66
msgid ""
"Windows: ``C:\\Python27\\Lib\\site-"
"packages\\openrem\\openremproject\\local_settings.py``"
msgstr ""

#: ../../release-0.8.0.rst:67
msgid ""
"Windows virtualenv: ``virtualenvfolder\\Lib\\site-"
"packages\\openrem\\openremproject\\local_settings.py``"
msgstr ""

#: ../../release-0.8.0.rst:70
msgid "Date format"
msgstr ""

#: ../../release-0.8.0.rst:71
msgid ""
"Set the date format for xlsx exports (need to check csv situation). Copy "
"the following code into your ``local_settings.py`` file if you want to "
"change it from ``dd/mm/yyy``:"
msgstr ""

#: ../../release-0.8.0.rst:82
msgid "Time zone and language"
msgstr ""

#: ../../release-0.8.0.rst:84
msgid ""
"Consider setting the timezone and language in ``local_settings.py``. See "
"``local_settings.py.example``."
msgstr ""

#: ../../release-0.8.0.rst:87
msgid "Add additional log file configuration"
msgstr ""

#: ../../release-0.8.0.rst:91
msgid ""
"If the configuration is not added for the new ``openrem_extractor.log`` "
"you will find it being created whereever you start the webserver from, "
"and starting the webserver may fail."
msgstr ""

#: ../../release-0.8.0.rst:94
msgid ""
"Add the new extractor log file configuration to the ``local_settings.py``"
" - you can copy the 'Logging configuration' section from  "
"``local_settings.py.example`` if you haven't made many changes to this "
"section. See the :ref:`local_settings_logfile` settings in the install "
"instructions."
msgstr ""

#: ../../release-0.8.0.rst:100
msgid ""
"If you are upgrading from an earlier beta with the Toshiba RDSR creation "
"logs defined, this has changed names and must be modified in "
"``local_settings.py`` before the migration below. It should be changed "
"to::"
msgstr ""

#: ../../release-0.8.0.rst:105
msgid "substituting ``INFO`` for whichever level of logging is desired."
msgstr ""

#: ../../release-0.8.0.rst:108
msgid "Adding legacy Toshiba CT functionality"
msgstr ""

#: ../../release-0.8.0.rst:110
msgid ""
"If you need to import data from older Toshiba CT scanners into OpenREM "
"then the following tools need to be available on the same server as "
"OpenREM:"
msgstr ""

#: ../../release-0.8.0.rst:113
msgid "The `Offis DICOM toolkit`_"
msgstr ""

#: ../../release-0.8.0.rst:114
msgid "`Java`_"
msgstr ""

#: ../../release-0.8.0.rst:115
msgid "pixelmed.jar from the `PixelMed Java DICOM Toolkit`_"
msgstr ""

#: ../../release-0.8.0.rst:117
msgid "The paths to these must be set in ``local_settings.py`` for your system:"
msgstr ""

#: ../../release-0.8.0.rst:130
msgid ""
"The example above is for Windows. On linux, if you have installed the "
"Offis DICOM toolkit with ``sudo apt install dcmtk`` or similar, you can "
"find the path for the configuration above using the command ``which "
"dcmconv``. This will be something like ``/usr/bin/dcmconv``, so the "
"``DCMTK_PATH`` would be ``'/usr/bin`` and the ``DCMCONV`` would be "
"``os.path.join(DCMTK_PATH, 'dcmconv')``. Similarly for ``DCMMKDIR`` and "
"``JAVA_EXE``, which might be ``/usr/bin/java``. The pixelmed.jar file "
"should be downloaded from the link above, and you will need to provide "
"the path to where you have saved it."
msgstr ""

#: ../../release-0.8.0.rst:139
msgid "Migrate the database"
msgstr ""

#: ../../release-0.8.0.rst:141
msgid "In a shell/command window, move into the openrem folder:"
msgstr ""

#: ../../release-0.8.0.rst:143
msgid "Ubuntu linux: ``/usr/local/lib/python2.7/dist-packages/openrem/``"
msgstr ""

#: ../../release-0.8.0.rst:144
msgid "Other linux: ``/usr/lib/python2.7/site-packages/openrem/``"
msgstr ""

#: ../../release-0.8.0.rst:145
msgid "Linux virtualenv: ``vitualenvfolder/lib/python2.7/site-packages/openrem/``"
msgstr ""

#: ../../release-0.8.0.rst:146
msgid "Windows: ``C:\\Python27\\Lib\\site-packages\\openrem\\``"
msgstr ""

#: ../../release-0.8.0.rst:147
msgid "Windows virtualenv: ``virtualenvfolder\\Lib\\site-packages\\openrem\\``"
msgstr ""

#: ../../release-0.8.0.rst:157
msgid "Update static files"
msgstr ""

#: ../../release-0.8.0.rst:159
msgid ""
"In the same shell/command window as you used above run the following "
"command to clear the static files belonging to your previous OpenREM "
"version and replace them with those belonging to the version you have "
"just installed (assuming you are using a production web server...):"
msgstr ""

#: ../../release-0.8.0.rst:169
msgid "Restart all the services"
msgstr ""

#: ../../release-0.8.0.rst:171
msgid "Follow the guide at :doc:`startservices`."
msgstr ""

