# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../import.rst:2
msgid "Importing data to OpenREM"
msgstr ""

#: ../../import.rst:5
msgid "From local DICOM files"
msgstr ""

#: ../../import.rst:7
msgid "If you have RDSRs, DX or MG images or Philips CT Dose Info images, you can import them directly into OpenREM:"
msgstr ""

#: ../../import.rst:14
msgid "If you want some examples, you can find the DICOM files that we use for the automated testing in the ``openrem/remapp/tests/test_files`` folder in your OpenREM installation."
msgstr ""

#: ../../import.rst:20
msgid "Direct from modalities"
msgstr ""

#: ../../import.rst:22
msgid "For production use, you will either need the modalities to send the RDSR or images directly to your OpenREM server using DICOM, or you will need to use query-retrieve to fetch the DICOM objects from the PACS or the modalities. In either of these situations, you will need to run a DICOM Store service on your OpenREM server."
msgstr ""

#: ../../import.rst:26
msgid "To get started, you can make use of the in-built DICOM store that can be configured from within OpenREM:"
msgstr ""

#: ../../import.rst:36
msgid "Third-party DICOM Stores"
msgstr ""

#: ../../import.rst:38
msgid "The DICOM store built in to OpenREM hasn't proved to be stable over the longer term with the current implementation and library that it depends on. This will be rectified in a future version, but for now we recommend you use a third-party DICOM store. Previous releases have recommended the Conquest DICOM server which is very good for this task. However, due to difficulties with installation on some platforms, we are now recommending the Orthanc DICOM server instead:"
msgstr ""

#: ../../import.rst:49
msgid "You ony need one of these - if you already have one installed it is probably easiest to stick to it."
msgstr ""

#: ../../import.rst:52
msgid "Query-retrieve from a PACS or similar"
msgstr ""

#: ../../import.rst:54
msgid "Before you can query-retrieve objects from a remote PACS, you need to do the following:"
msgstr ""

#: ../../import.rst:56
msgid "Create a DICOM Store service to receive the DICOM objects - see :ref:`directfrommodalities` above."
msgstr ""

#: ../../import.rst:57
msgid "Configure OpenREM with the settings for the remote query-retrieve server:"
msgstr ""

#: ../../import.rst:64
msgid "Configure the settings of your DICOM store service on the PACS"
msgstr ""

#: ../../import.rst:66
msgid "Learn how to use it:"
msgstr ""
