# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releasenotes.rst:3
msgid "Release Notes and Change Log"
msgstr ""

#: ../../releasenotes.rst:7
msgid "Version history change log"
msgstr ""

#: ../../releasenotes.rst:16
msgid "Release notes and upgrade instructions"
msgstr ""

#: ../../releasenotes.rst:18
msgid "Each release comes with specific upgrade instructions, so please follow the links below for the appropriate version."
msgstr ""

#: ../../releasenotes.rst:22
msgid "Version specific information"
msgstr ""

#: ../../releasenotes.rst:25
msgid ":doc:`release-1.0.0` (this release)"
msgstr ""

#: ../../releasenotes.rst:49
msgid "Contributing authors"
msgstr ""

#: ../../releasenotes.rst:51
msgid "Many people have contributed to OpenREM - either with code, documentation, bugs, examples or ideas, including:"
msgstr ""

#: ../../releasenotes.rst:53
msgid "`BjörnAlbers <https://bitbucket.org/bjoernalbers/>`_"
msgstr ""

#: ../../releasenotes.rst:54
msgid "`Erlend Andersen <https://bitbuckeet.org/erlend_andersen/>`_"
msgstr ""

#: ../../releasenotes.rst:55
msgid "`Njål Brekke <https://bitbuckeet.org/elenhinan/>`_"
msgstr ""

#: ../../releasenotes.rst:56
msgid "Elly Castellano"
msgstr ""

#: ../../releasenotes.rst:57
msgid "`Jonathan Cole <https://bitbucket.org/jacole/>`_"
msgstr ""

#: ../../releasenotes.rst:58
msgid "`Jamie Dormand <https://bitbucket.org/jamiedormand/>`_"
msgstr ""

#: ../../releasenotes.rst:59
msgid "`Ben Earner <https://bitbucket.org/bearner/>`_"
msgstr ""

#: ../../releasenotes.rst:60
msgid "`Daniel Gordon <https://bitbucket.org/dan_gordon/>`_"
msgstr ""

#: ../../releasenotes.rst:61
msgid "`Hamid Khosravi <https://bitbucket.org/hrkhosravi/>`_"
msgstr ""

#: ../../releasenotes.rst:62
msgid "`Laurence King <https://bitbucket.org/LaurenceKing/>`_"
msgstr ""

#: ../../releasenotes.rst:63
msgid "`Eivind Larsen <https://bitbucket.org/leivind/>`_"
msgstr ""

#: ../../releasenotes.rst:64
msgid "`John Loveland <https://bitbucket.org/JLMPO/>`_"
msgstr ""

#: ../../releasenotes.rst:65
msgid "`Ed McDonagh <https://bitbucket.org/edmcdonagh/>`_"
msgstr ""

#: ../../releasenotes.rst:66
msgid "`Richard Miles <https://bitbucket.org/r89m/>`_"
msgstr ""

#: ../../releasenotes.rst:67
msgid "`Luuk Oostveen <https://bitbucket.org/LuukO/>`_"
msgstr ""

#: ../../releasenotes.rst:68
msgid "`David Platten <https://bitbucket.org/dplatten/>`_"
msgstr ""

#: ../../releasenotes.rst:69
msgid "`Richard Raynor <https://bitbucket.org/RaynorR/>`_"
msgstr ""

#: ../../releasenotes.rst:70
msgid "`Erik-Jan Rijkhorst <https://bitbucket.org/rijkhorst/>`_"
msgstr ""

#: ../../releasenotes.rst:71
msgid "`Arnold Schilham <https://bitbucket.org/asch99/>`_"
msgstr ""

#: ../../releasenotes.rst:72
msgid "`Tim de Wit <https://bitbucket.org/tcdewit/>`_"
msgstr ""
