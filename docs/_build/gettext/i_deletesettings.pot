# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../i_deletesettings.rst:3
msgid "Delete objects configuration"
msgstr ""

#: ../../i_deletesettings.rst:5
msgid "OpenREM is able to automatically delete DICOM objects if they can't be used by OpenREM or if they have been processed. This has the following advantages:"
msgstr ""

#: ../../i_deletesettings.rst:8
msgid "The server doesn't need to have much storage space"
msgstr ""

#: ../../i_deletesettings.rst:9
msgid "It can help with information governance if the database is set to not store patient identifiable data (see :doc:`patientid`)"
msgstr ""

#: ../../i_deletesettings.rst:13
msgid "If OpenREM is set to delete objects and you pass a local file to OpenREM using the command line, the source file will be deleted (as long as the filesystem permissions allow)."
msgstr ""

#: ../../i_deletesettings.rst:18
msgid "Configure what is deleted"
msgstr ""

#: ../../i_deletesettings.rst:25
msgid "The ``Config`` menu"
msgstr ""

#: ../../i_deletesettings.rst:27
msgid "Use the ``Config`` menu and select ``DICOM object deletion``:"
msgstr ""

#: ../../i_deletesettings.rst:29
msgid "This will open the configuration page:"
msgstr ""

#: ../../i_deletesettings.rst:36
msgid "Modify DICOM object deletion policy"
msgstr ""

#: ../../i_deletesettings.rst:42
msgid "The initial settings are to not delete anything. However, you are likely to want to delete objects that don't match any import filters, and also to delete images such as mammo, DX and Philips CT, as these will take up space much more quickly than the radiation dose structured reports."
msgstr ""

#: ../../i_deletesettings.rst:48
msgid "Reviewing the settings"
msgstr ""

#: ../../i_deletesettings.rst:50
msgid "When you have set your preferences, you will be redirected to the DICOM network configuration page, where at the bottom you can review the current settings:"
msgstr ""

#: ../../i_deletesettings.rst:57
msgid "Deletion policies can be reviewed on the DICOM network configuration page"
msgstr ""

#: ../../i_deletesettings.rst:59
msgid "More information about the DICOM network configuration can be found on the :ref:`directfrommodalities` page."
msgstr ""
