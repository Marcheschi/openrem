# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../release-0.5.1.rst:3
msgid "OpenREM Release Notes version 0.5.1"
msgstr ""

#: ../../release-0.5.1.rst:7
msgid "Headline changes"
msgstr ""

#: ../../release-0.5.1.rst:9
msgid "Major database modification to remove table name length errors"
msgstr ""

#: ../../release-0.5.1.rst:10
msgid "Extended the field value lengths to better incorporate all possible values and decimal places"
msgstr ""

#: ../../release-0.5.1.rst:11
msgid "Improved import of grid and filter information from DX images"
msgstr ""

#: ../../release-0.5.1.rst:12
msgid "Improved DX summary and detail web pages"
msgstr ""

#: ../../release-0.5.1.rst:13
msgid "Any item in a row can now be clicked to move between the home and filtered pages"
msgstr ""

#: ../../release-0.5.1.rst:17
msgid "Upgrades: Convert to South"
msgstr ""

#: ../../release-0.5.1.rst:19
msgid "**Always make sure you have converted your database to South before attempting an upgrade**"
msgstr ""

#: ../../release-0.5.1.rst:21
msgid "Quick reminder of how, if you haven't done it already"
msgstr ""

#: ../../release-0.5.1.rst:35
msgid "Upgrading from before 0.5.0"
msgstr ""

#: ../../release-0.5.1.rst:38
msgid "Upgrading from version 0.3.9 or earlier"
msgstr ""

#: ../../release-0.5.1.rst:40
#: ../../release-0.5.1.rst:119
#: ../../release-0.5.1.rst:200
#: ../../release-0.5.1.rst:226
msgid "Back up your database"
msgstr ""

#: ../../release-0.5.1.rst:42
#: ../../release-0.5.1.rst:121
#: ../../release-0.5.1.rst:202
#: ../../release-0.5.1.rst:228
msgid "For PostgreSQL you can refer to :doc:`backupRestorePostgreSQL`"
msgstr ""

#: ../../release-0.5.1.rst:43
#: ../../release-0.5.1.rst:122
#: ../../release-0.5.1.rst:203
#: ../../release-0.5.1.rst:229
msgid "For a non-production SQLite3 database, simply make a copy of the database file"
msgstr ""

#: ../../release-0.5.1.rst:45
msgid "``pip install openrem==0.4.2``"
msgstr ""

#: ../../release-0.5.1.rst:46
#: ../../release-0.5.1.rst:177
msgid "Migrate the schema"
msgstr ""

#: ../../release-0.5.1.rst:57
msgid "When South has considered the changes to the schema, you will see the following message::"
msgstr ""

#: ../../release-0.5.1.rst:67
msgid "As per the final line above, please select option 3, and then execute the migration:"
msgstr ""

#: ../../release-0.5.1.rst:79
msgid "Create and populate the database settings in the new ``local_settings.py`` file"
msgstr ""

#: ../../release-0.5.1.rst:81
msgid "The ``openrem/openrem`` folder can be found at:"
msgstr ""

#: ../../release-0.5.1.rst:92
msgid "In the ``openrem/openrem`` folder, create a new file called ``local_settings.py`` and copy the `contents of this link <https://bitbucket.org/openrem/openrem/raw/a37540ba88a5e9b383cf0ea03a3e77fb35638f43/openrem/openremproject/local_settings.py.example>`_ into a the file and save it. Alternatively, rename ``local_settings.py.example`` to ``local_settings.py`` - this is an older version of the file."
msgstr ""

#: ../../release-0.5.1.rst:97
msgid "Copy the database details from ``settings.py`` into ``local_settings.py``"
msgstr ""

#: ../../release-0.5.1.rst:99
msgid "Change the secret key - you can use http://www.miniwebtool.com/django-secret-key-generator/ to generate a new one"
msgstr ""

#: ../../release-0.5.1.rst:100
msgid "Move the existing ``settings.py`` out of the python directories (delete or move somewhere as a backup)"
msgstr ""

#: ../../release-0.5.1.rst:101
msgid "Rename the ``settings.py.new`` to ``settings.py``"
msgstr ""

#: ../../release-0.5.1.rst:102
msgid "Restart your webserver to check everything looks ok"
msgstr ""

#: ../../release-0.5.1.rst:103
msgid "Add some users"
msgstr ""

#: ../../release-0.5.1.rst:105
msgid "Go to the admin interface (eg http://localhost:8000/admin) and log in with the user created when you originally created the database (the ``manage.py syncdb`` command - *Do you want to create a superuser*)"
msgstr ""

#: ../../release-0.5.1.rst:108
msgid "Create some users and add them to the appropriate groups (if there are no groups, go to the OpenREM homepage and they should be there when you go back to admin)."
msgstr ""

#: ../../release-0.5.1.rst:111
msgid "``viewgroup`` can browse the data only"
msgstr ""

#: ../../release-0.5.1.rst:112
msgid "``exportgroup`` can do as view group plus export data to a spreadsheet, and will be able to import height and weight data in due course (See `Issue #21 <https://bitbucket.org/openrem/openrem/issue/21/>`_)"
msgstr ""

#: ../../release-0.5.1.rst:113
msgid "``admingroup`` can delete studies in addition to anything the export group can do"
msgstr ""

#: ../../release-0.5.1.rst:117
msgid "Upgrading from versions 0.4.0 - 0.4.2"
msgstr ""

#: ../../release-0.5.1.rst:124
msgid "Install version 0.5.0"
msgstr ""

#: ../../release-0.5.1.rst:126
msgid "``pip install openrem==0.5.0``"
msgstr ""

#: ../../release-0.5.1.rst:128
msgid "Install RabbitMQ"
msgstr ""

#: ../../release-0.5.1.rst:130
msgid "Linux - Follow the guide at http://www.rabbitmq.com/install-debian.html"
msgstr ""

#: ../../release-0.5.1.rst:131
msgid "Windows - Follow the guide at http://www.rabbitmq.com/install-windows.html"
msgstr ""

#: ../../release-0.5.1.rst:133
msgid "Move ``local_settings.py`` details from ``openrem`` to ``openremproject``"
msgstr ""

#: ../../release-0.5.1.rst:135
msgid "The inner ``openrem`` Django project folder has now been renamed ``openremproject`` The customised ``local_settings.py`` file and the ``wsgi.py`` file have remain in the old ``openrem`` folder. The ``openrem/openrem`` folder can be found as detailed in the upgrade from '0.3.9 or earlier' instructions above, and the new ``openrem/openremproject`` folder is in the same place."
msgstr ""

#: ../../release-0.5.1.rst:140
msgid "Move ``local_settings.py`` to ``openremproject``. If you have kept the older local_settings file, you may like to instead rename the ``local_settings.py.example`` file instead, then transfer the database settings and change the secret key."
msgstr ""

#: ../../release-0.5.1.rst:144
msgid "Set the path for ``MEDIA_ROOT``. The webserver needs to be able to write to this location - it is where OpenREM will store export files etc so that they can be downloaded. For suggestions, see the main _install instructions."
msgstr ""

#: ../../release-0.5.1.rst:147
msgid "Set ``ALLOWED_HOSTS``. For details see the `Django docs <https://docs.djangoproject.com/en/1.6/ref/settings/#allowed-hosts>`_ A ``'*'`` allows any host - see the Django docs for the risk of this."
msgstr ""

#: ../../release-0.5.1.rst:150
msgid "Move ``wsgi.py`` from ``openrem`` to ``openremproject`` or rename ``wsgi.py.example`` in ``openremproject``"
msgstr ""

#: ../../release-0.5.1.rst:152
msgid "If you haven't edited it, simply rename the new version in ``openremproject``. Otherwise, move the old version and edit the following line as follows:"
msgstr ""

#: ../../release-0.5.1.rst:163
msgid "Tidying up - you should delete the old ``openrem`` folder - you might like to take a backup first!"
msgstr ""

#: ../../release-0.5.1.rst:165
msgid "Update web server configuration"
msgstr ""

#: ../../release-0.5.1.rst:167
msgid "The configuration of the webserver will need to be updated to reflect the new location for the ``settings.py`` file and the ``wsgi.py`` file."
msgstr ""

#: ../../release-0.5.1.rst:170
msgid "If you are using the built-in test webserver, static files will no-longer be served unless you use the ``insecure`` option:"
msgstr ""

#: ../../release-0.5.1.rst:191
msgid "After restarting the webserver, you should now have OpenREM 0.5.0 up and running. If you wish to test export functionality at this stage, start the Celery task queue - instructions in the :doc:`install` docs or at the end of this guide."
msgstr ""

#: ../../release-0.5.1.rst:195
msgid "Now move to `Upgrading from version 0.5.0`_."
msgstr ""

#: ../../release-0.5.1.rst:198
msgid "Upgrading from version 0.4.3"
msgstr ""

#: ../../release-0.5.1.rst:205
msgid "The 0.5.1 upgrade `must` be made from a 0.5.0 database, so a schema migration is required:"
msgstr ""

#: ../../release-0.5.1.rst:224
msgid "Upgrading from version 0.5.0"
msgstr ""

#: ../../release-0.5.1.rst:231
msgid "Install 0.5.1:"
msgstr ""

#: ../../release-0.5.1.rst:237
msgid "Find out how many migration files you have"
msgstr ""

#: ../../release-0.5.1.rst:239
msgid "Method 1:"
msgstr ""

#: ../../release-0.5.1.rst:241
msgid "Use a file browser or terminal to list the contents of the ``migrations`` folder, eg:"
msgstr ""

#: ../../release-0.5.1.rst:252
msgid "Method 2:"
msgstr ""

#: ../../release-0.5.1.rst:254
msgid "Use the Django ``manage.py`` program to list the existing migrations:"
msgstr ""

#: ../../release-0.5.1.rst:265
msgid "The output should look something like this - there can be any number of existing migrations (though 0001_initial will always be present)::"
msgstr ""

#: ../../release-0.5.1.rst:280
msgid "Rename the two 051 migration files to follow on from the existing migrations, for example ``0008_051schemamigration.py`` and ``0009_051datamigration.py`` for the existing migrations above, or ``0002_051schemamigration.py`` and ``0003_051datamigration.py`` if the only migration is the initial migration. The ``051schemamigration`` **must** come before the ``051datamigration``."
msgstr ""

#: ../../release-0.5.1.rst:285
msgid "If you are using linux, you might like to do it like this (from within the ``openrem`` folder):"
msgstr ""

#: ../../release-0.5.1.rst:292
msgid "If you now re-run ``migrate --list remapp`` you should get a listing with the ``051schemamigration`` and the ``051datamigration`` listed at the end::"
msgstr ""

#: ../../release-0.5.1.rst:306
msgid "The star indicates that a migration has already been completed. If you have any that are not completed apart from the ``051schemamigration`` and the ``051datamigration`` then please resolve these first."
msgstr ""

#: ../../release-0.5.1.rst:309
msgid "Now execute the migrations:"
msgstr ""

#: ../../release-0.5.1.rst:322
msgid "Restart the web server"
msgstr ""

#: ../../release-0.5.1.rst:324
msgid "If you are using the built-in test web server (`not for production use`)::"
msgstr ""

#: ../../release-0.5.1.rst:328
msgid "Otherwise restart using the command for your web server"
msgstr ""

#: ../../release-0.5.1.rst:331
msgid "Restart the Celery task queue"
msgstr ""

#: ../../release-0.5.1.rst:333
msgid "For testing, in a new shell:"
msgstr ""

#: ../../release-0.5.1.rst:347
msgid "For production use, see http://celery.readthedocs.org/en/latest/tutorials/daemonizing.html"
msgstr ""
