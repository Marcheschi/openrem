# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../virtual_directory.rst:3
msgid "Running the OpenREM website in a virtual directory"
msgstr ""

#: ../../virtual_directory.rst:5
msgid "If you want to run the OpenREM in a virtual directory (like http://server/dms/) you need to configure this in your web server configuration as well as in the OpenREM configuration."
msgstr ""

#: ../../virtual_directory.rst:8
msgid "The following steps are necessary:"
msgstr ""

#: ../../virtual_directory.rst:10
msgid "Configure virtual directory settings in the Docker ``.env.prod`` file"
msgstr ""

#: ../../virtual_directory.rst:11
msgid "Update Nginx webserver configuration"
msgstr ""

#: ../../virtual_directory.rst:12
msgid "Update the ``reverse.js`` file"
msgstr ""

#: ../../virtual_directory.rst:13
msgid "Restart the containers"
msgstr ""

#: ../../virtual_directory.rst:16
msgid "Docker setup"
msgstr ""

#: ../../virtual_directory.rst:18
msgid "Stop the containers if they are running before changing the configuration, using a shell (command prompt) in the Docker OpenREM installation folder"
msgstr ""

#: ../../virtual_directory.rst:26
msgid "Configure virtual directory settings in .env.prod"
msgstr ""

#: ../../virtual_directory.rst:28
msgid "Django needs to know the virtual directory name and which URLs the static and media files are served from."
msgstr ""

#: ../../virtual_directory.rst:30
msgid "Edit ``.env.prod``, uncomment the following lines (remove the ``#``) and set them as appropriate. For example, to serve the website from a subfolder/virtual directory named ``dms``:"
msgstr ""

#: ../../virtual_directory.rst:41
#: ../../virtual_directory.rst:112
msgid "Modify webserver configuration"
msgstr ""

#: ../../virtual_directory.rst:43
msgid "Edit ``nginx-conf/conf.d/openrem.conf`` to update the locations — again using the example virtual directory ``dms``:"
msgstr ""

#: ../../virtual_directory.rst:62
msgid "Start the containers"
msgstr ""

#: ../../virtual_directory.rst:69
#: ../../virtual_directory.rst:132
msgid "Update reverse.js"
msgstr ""

#: ../../virtual_directory.rst:71
msgid "The static reverse.js file should be updated in order to change the URLs in the static javascript files."
msgstr ""

#: ../../virtual_directory.rst:73
msgid "Open a shell (command prompt) and navigate to the Docker OpenREM installation folder"
msgstr ""

#: ../../virtual_directory.rst:81
msgid "Test!"
msgstr ""

#: ../../virtual_directory.rst:83
msgid "You should now be able to reach the OpenREM interface using the virtual directory address."
msgstr ""

#: ../../virtual_directory.rst:87
msgid "Non-Docker Linux install"
msgstr ""

#: ../../virtual_directory.rst:95
msgid "Update local_settings.py"
msgstr ""

#: ../../virtual_directory.rst:97
msgid "Update ``local_settings.py`` with the same variables as in the ``.env.prod`` file. If the values aren't in your copy of the file just add them in:"
msgstr ""

#: ../../virtual_directory.rst:141
msgid "Restart the services"
msgstr ""
