# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../release-1.0.0.rst:3
msgid "Upgrade to OpenREM 1.0.0"
msgstr ""

#: ../../release-1.0.0.rst:7
msgid "Headline changes"
msgstr ""

#: ../../release-1.0.0.rst:9
msgid "Python 3"
msgstr ""

#: ../../release-1.0.0.rst:10
msgid "Django 2.2"
msgstr ""

#: ../../release-1.0.0.rst:11
msgid "Docker"
msgstr ""

#: ../../release-1.0.0.rst:13
msgid "Performing physician added to standard fluoroscopy exports (:issue:`840`)"
msgstr ""

#: ../../release-1.0.0.rst:17
msgid "Upgrade preparation"
msgstr ""

#: ../../release-1.0.0.rst:19
msgid "These instructions assume you are upgrading from 0.10.0."
msgstr ""

#: ../../release-1.0.0.rst:20
msgid "**Upgrades from 0.9.1 or earlier should review** :doc:`upgrade_previous_0.10.0`. -- needs changing"
msgstr ""

#: ../../release-1.0.0.rst:31
msgid "Upgrade process from a PostgresQL database"
msgstr ""

#: ../../release-1.0.0.rst:34
msgid "Establish existing database details"
msgstr ""

#: ../../release-1.0.0.rst:36
msgid "Review the current ``local_settings.py`` for the database settings and location of the ``MEDIA_ROOT`` folder. The file is in:"
msgstr ""

#: ../../release-1.0.0.rst:39
msgid "Ubuntu linux: ``/usr/local/lib/python2.7/dist-packages/openrem/openremproject/local_settings.py``"
msgstr ""

#: ../../release-1.0.0.rst:40
msgid "Other linux: ``/usr/lib/python2.7/site-packages/openrem/openremproject/local_settings.py``"
msgstr ""

#: ../../release-1.0.0.rst:41
msgid "Linux virtualenv: ``vitualenvfolder/lib/python2.7/site-packages/openrem/openremproject/local_settings.py``"
msgstr ""

#: ../../release-1.0.0.rst:42
msgid "Windows: ``C:\\Python27\\Lib\\site-packages\\openrem\\openremproject\\local_settings.py``"
msgstr ""

#: ../../release-1.0.0.rst:43
msgid "Windows virtualenv: ``virtualenvfolder\\Lib\\site-packages\\openrem\\openremproject\\local_settings.py``"
msgstr ""

#: ../../release-1.0.0.rst:47
msgid "Export the database"
msgstr ""

#: ../../release-1.0.0.rst:49
msgid "Open a command line window"
msgstr ""

#: ../../release-1.0.0.rst:50
msgid "Windows: go to Postgres bin folder, for example:"
msgstr ""

#: ../../release-1.0.0.rst:56
msgid "Dump the database:"
msgstr ""

#: ../../release-1.0.0.rst:58
msgid "Use the username (``-U openremuser``) and database name (``-d openremdb``) from ``local_settings.py``"
msgstr ""

#: ../../release-1.0.0.rst:59
msgid "Use the password from ``local_settings.py`` when prompted"
msgstr ""

#: ../../release-1.0.0.rst:60
msgid "For linux, the command is ``pg_dump`` (no ``.exe``)"
msgstr ""

#: ../../release-1.0.0.rst:61
msgid "Set the path to somewhere suitable to dump the exported database file"
msgstr ""

#: ../../release-1.0.0.rst:68
msgid "Set up the new installation"
msgstr ""

#: ../../release-1.0.0.rst:72
msgid "Install Docker"
msgstr ""

#: ../../release-1.0.0.rst:73
msgid "Download and extract https://bitbucket.org/openrem/docker/get/develop.zip and open a shell (command window) in the new folder"
msgstr ""

#: ../../release-1.0.0.rst:75
msgid "Customise variables in ``.env.prod`` and in the ``orthanc_1`` section in ``docker-compose.yml`` as necessary. A full description of the options are found in:"
msgstr ""

#: ../../release-1.0.0.rst:84
msgid "Start the containers with:"
msgstr ""

#: ../../release-1.0.0.rst:90
msgid "Copy the database backup to the postgres docker container and import it. If you have changed the database variables, ensure that:"
msgstr ""

#: ../../release-1.0.0.rst:93
msgid "the database user (``-U openremuser``) matches ``POSTGRES_USER`` in ``.env.prod``"
msgstr ""

#: ../../release-1.0.0.rst:94
msgid "the database name (``-d openrem_prod``) matches ``POSTGRES_DB`` in ``.env.prod``"
msgstr ""

#: ../../release-1.0.0.rst:96
msgid "They don't have to match the old database settings. The filename in both commands (``openremdump.bak``) should match your backup filename."
msgstr ""

#: ../../release-1.0.0.rst:107
msgid "It is normal to get an error about the public schema, for example:"
msgstr ""

#: ../../release-1.0.0.rst:118
msgid "Rename the 0.10 upgrade migration file, migrate the database (the steps and fakes are required as it is not a new database), and create the static files:"
msgstr ""

#: ../../release-1.0.0.rst:145
#: ../../release-1.0.0.rst:293
msgid "Generate translation binary files"
msgstr ""

#: ../../release-1.0.0.rst:151
msgid "Copy in any existing skin dose map pickle files from your existing ``MEDIA_ROOT/skin_maps`` folder (optional, they can be calculated again):"
msgstr ""

#: ../../release-1.0.0.rst:158
msgid "The new OpenREM installation should now be ready to be used."
msgstr ""

#: ../../release-1.0.0.rst:162
msgid "Upgrading an OpenREM server with no internet access"
msgstr ""

#: ../../release-1.0.0.rst:164
msgid "Follow the instructions found at :doc:`upgrade-offline`, before returning here to update the configuration, migrate the database and complete the upgrade."
msgstr ""

#: ../../release-1.0.0.rst:169
msgid "Upgrading an OpenREM server that uses a different database"
msgstr ""

#: ../../release-1.0.0.rst:175
msgid "Upgrading without using Docker - linux only"
msgstr ""

#: ../../release-1.0.0.rst:177
msgid "Upgrading without using Docker is not recommended, and not supported on Windows. Instructions are only provided for Linux and assume a configuration similar to the 'One page complete Ubuntu install' provided with release 0.8.1 and later."
msgstr ""

#: ../../release-1.0.0.rst:182
msgid "Preparation"
msgstr ""

#: ../../release-1.0.0.rst:184
msgid "Back up the database:"
msgstr ""

#: ../../release-1.0.0.rst:190
msgid "Stop any Celery workers, Flower and Gunicorn, disable DICOM Store SCP:"
msgstr ""

#: ../../release-1.0.0.rst:199
msgid "Install Python 3.8 and create a new virtualenv:"
msgstr ""

#: ../../release-1.0.0.rst:212
msgid "Install the new version of OpenREM"
msgstr ""

#: ../../release-1.0.0.rst:223
msgid "Update the local_settings.py file"
msgstr ""

#: ../../release-1.0.0.rst:225
msgid "Remove the first line ``LOCAL_SETTINGS = True``"
msgstr ""

#: ../../release-1.0.0.rst:226
msgid "Change second line to ``from .settings import *``"
msgstr ""

#: ../../release-1.0.0.rst:227
msgid "Compare file to local_settings.py.example to see if there are other sections that should be updated"
msgstr ""

#: ../../release-1.0.0.rst:230
msgid "Migrate the database"
msgstr ""

#: ../../release-1.0.0.rst:232
msgid "In a shell/command window, move into the ``openrem`` folder:"
msgstr ""

#: ../../release-1.0.0.rst:238
msgid "Prepare the migrations folder:"
msgstr ""

#: ../../release-1.0.0.rst:240
msgid "Delete everything except ``__init__.py`` and ``0001_initial.py.1-0-upgrade`` in ``remapp/migrations``"
msgstr ""

#: ../../release-1.0.0.rst:241
msgid "Rename ``0001_initial.py.1-0-upgrade`` to ``0001_initial.py``"
msgstr ""

#: ../../release-1.0.0.rst:249
msgid "Migrate the database:"
msgstr ""

#: ../../release-1.0.0.rst:269
msgid "Update static files"
msgstr ""

#: ../../release-1.0.0.rst:275
msgid "Virtual directory users"
msgstr ""

#: ../../release-1.0.0.rst:277
msgid "If you are running your website in a virtual directory, you also have to update the reverse.js file. To get the file in the correct path, take care that you insert just after the declaration of ``STATIC_ROOT`` the following line in your ``local_settings.py`` (see also the sample ``local_settings.py.example``):"
msgstr ""

#: ../../release-1.0.0.rst:285
msgid "To update the reverse.js file execute the following command:"
msgstr ""

#: ../../release-1.0.0.rst:291
msgid "See  :doc:`virtual_directory` for more details."
msgstr ""

#: ../../release-1.0.0.rst:300
msgid "Update all the services configurations"
msgstr ""

#: ../../release-1.0.0.rst:302
msgid "Edit the Gunicorn systemd file ``WorkingDirectory`` and ``ExecStart``:"
msgstr ""

#: ../../release-1.0.0.rst:316
msgid "Edit the Celery configuration file ``CELERY_BIN``:"
msgstr ""

#: ../../release-1.0.0.rst:326
msgid "Edit the Celery systemd file ``WorkingDirectory``:"
msgstr ""

#: ../../release-1.0.0.rst:336
msgid "Edit the Flower systemd file ``WorkingDirectory``:"
msgstr ""

#: ../../release-1.0.0.rst:347
msgid "Reload systemd and restart the services"
msgstr ""
