# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../netdicom-qr.rst:3
msgid "DICOM Query Retrieve Service"
msgstr ""

#: ../../netdicom-qr.rst:6
msgid "To query retrieve dose related objects from a remote server, you need to review the :doc:`netdicom-store` documents first to make sure you have created a DICOM Store node which will import objects to OpenREM."
msgstr ""

#: ../../netdicom-qr.rst:9
msgid "You will also need to set up the remote server to allow you to query-retrieve using it - the remote server will need to be configured with details of the store node that you have configured."
msgstr ""

#: ../../netdicom-qr.rst:14
msgid "Query-retrieve using the web interface"
msgstr ""

#: ../../netdicom-qr.rst:22
msgid "Figure 1: Import Query-Retrieve menu"
msgstr ""

#: ../../netdicom-qr.rst:24
msgid "On the Imports menu, select ``Query remote server`` - see figure 1. If the menu isn't there, you need to check your user permissions -- see :ref:`user-settings` for details."
msgstr ""

#: ../../netdicom-qr.rst:26
msgid "Each configured query-retrieve node and each local store node is automatically tested to make sure they respond to a DICOM echo - the results are presented at the top of the page. See figure 2 for an example."
msgstr ""

#: ../../netdicom-qr.rst:35
msgid "Figure 2: Local and remote QR statuses"
msgstr ""

#: ../../netdicom-qr.rst:37
msgid "Select the desired **remote host**, ie the PACS or modality you wish to query."
msgstr ""

#: ../../netdicom-qr.rst:38
msgid "Select the local **store node** you want to retrieve to."
msgstr ""

#: ../../netdicom-qr.rst:39
msgid "Select **which modalities** you want to query for - at least one must be ticked."
msgstr ""

#: ../../netdicom-qr.rst:40
msgid "Select a **date range** - the wider this is, the more stress the query will place on the remote server, and the higher the likelyhood of the query being returned with zero results (a common configuration on the remote host to prevent large database queries affecting other services). Defaults to 'from yesterday'."
msgstr ""

#: ../../netdicom-qr.rst:43
msgid "If you wish to **exclude studies** based on their study description, enter the text here. Add several terms by separating them with a comma. One example would be to exclude any studies with ``imported`` in the study description, if your institution modifies this field on import. The matching is case-insensitive."
msgstr ""

#: ../../netdicom-qr.rst:46
msgid "Alternatively, you might want to only **keep studies** with particular terms in the study description. If so, enter them in the next box, comma separated."
msgstr ""

#: ../../netdicom-qr.rst:48
msgid "You can also **exclude studies by station name**, or only keep them if they match the station name. This is only effective if the remote system (the PACS) supports sending back station name information."
msgstr ""

#: ../../netdicom-qr.rst:52
msgid "Advanced query options"
msgstr ""

#: ../../netdicom-qr.rst:54
msgid "**Attempt to get Toshiba dose images** *default not ticked*: If you have done the extra installation and configuration required for creating RDSRs from older Toshiba scanners, then you can tick this box for `CT` searches to get the images needed for this process. See the logic description below for details."
msgstr ""

#: ../../netdicom-qr.rst:57
msgid "**Ignore studies already in the database** *default ticked*: By default OpenREM will attempt to avoid downloading any DICOM objects (RDSRs or images) that have already been imported into the database. Untick this box to override that behaviour and download all suitable objects. See the logic description below for details."
msgstr ""

#: ../../netdicom-qr.rst:60
msgid "**Include SR only studies** *default not ticked*: If you have a DICOM store with only the radiation dose structured reports (RDSR) in, or a mix of whole studies and RDSRs without the corresponding study, then tick this box. Any studies with images and RDSRS will be ignored (they can be found without this option). If this box is ticked any modality choices will be ignored."
msgstr ""

#: ../../netdicom-qr.rst:64
msgid "**Get SR series that return nothing at image level query** *default not ticked*: If you have a DICOM store with SR series that you know contain RDSR objects, but when queried your store says they are empty, then check this box. If this behaviour is found, a message will be added to the ``openrem_qr.log`` at ``INFO`` level with the phrase ``Try '-emptysr' option?``. With the box checked the query will assume any SR series found contains an RDSR. Warning: with this behavior, any non-RDSR structured report series (such as a radiologists report encoded as a structured report) will be retrieved instead of images that could actually be used (for example with mammography and digital radiographs). Therefore this option should be used with caution!"
msgstr ""

#: ../../netdicom-qr.rst:72
msgid "When you have finished the query parameters, click ``Submit``"
msgstr ""

#: ../../netdicom-qr.rst:75
msgid "Review and retrieve"
msgstr ""

#: ../../netdicom-qr.rst:77
msgid "The progress of the query is reported on the right hand side. If nothing happens, ask the administrator to check if the celery queue is running."
msgstr ""

#: ../../netdicom-qr.rst:80
msgid "Once all the responses have been purged of unwanted modalities, study descriptions or study UIDs, the number of studies of each type will be displayed and a button appears. Click ``Retrieve`` to request the remote server send the selected objects to your selected Store node. This will be based on your original selection - changing the node on the left hand side at this stage will have no effect."
msgstr ""

#: ../../netdicom-qr.rst:85
msgid "The progress of the retrieve is displayed in the same place until the retrieve is complete."
msgstr ""

#: ../../netdicom-qr.rst:91
msgid "Query-retrieve using the command line interface"
msgstr ""

#: ../../netdicom-qr.rst:93
msgid "In a command window/shell, ``python openrem_qr.py -h`` should present you with the following output:"
msgstr ""

#: ../../netdicom-qr.rst:142
msgid "As an example, if you wanted to query the PACS for DX images on the 5th and 6th April 2010 with any study descriptions including ``imported`` excluded, first you need to know the database IDs of the remote node and the local node you want the images sent to. To find these, go to the :doc:`netdicom-nodes` page where the database ID is listed among the other details for each node."
msgstr ""

#: ../../netdicom-qr.rst:147
msgid "Assuming the PACS database ID is 2, and the store node ID is 1, the command would look something like:"
msgstr ""

#: ../../netdicom-qr.rst:153
msgid "If you want to do this regularly to catch new studies, you might like to use a script something like this on linux:"
msgstr ""

#: ../../netdicom-qr.rst:166
msgid "This script could be run once an hour using a cron job. By asking for the date an hour ago, you shouldn't miss exams taking place in the last hour of the day."
msgstr ""

#: ../../netdicom-qr.rst:169
msgid "A similar script could be created as a batch file or PowerShell script on Windows and run using the scheduler. An example PowerShell script is shown below:"
msgstr ""

#: ../../netdicom-qr.rst:181
msgid "The above PowerShell script could be run on a regular basis by adding a task to the Windows ``Task Scheduler`` that executes the ``powershell`` program with an argument of ``-file C:\\path\\to\\script.ps1``."
msgstr ""

#: ../../netdicom-qr.rst:185
msgid "Querying with time range"
msgstr ""

#: ../../netdicom-qr.rst:187
msgid "*New to OpenREM 0.9.0*"
msgstr ""

#: ../../netdicom-qr.rst:189
msgid "It is now possible to query for studies in a time window when using query-retrieve from the command line (web interface version will be introduced later). This can be particularly useful where PACS query responses are limited or null if the query matches too many studies."
msgstr ""

#: ../../netdicom-qr.rst:193
msgid "Using the ``--tfrom``/``-tf`` and/or the ``--tuntil``/``-tt`` arguments are only allowed if ``--single_date``/``-sd`` argument is used."
msgstr ""

#: ../../netdicom-qr.rst:196
msgid "Note: ``-sd 2018-03-19`` is the same as using ``-f 2018-03-19 -t 2018-03-19``, and can be used without the time arguments."
msgstr ""

#: ../../netdicom-qr.rst:199
msgid "``-tf`` used without ``-tt`` will search from ``tf`` until 23.59 that day."
msgstr ""

#: ../../netdicom-qr.rst:200
msgid "``-tt`` used without ``-tf`` will search from 00.00 to ``tt`` that day."
msgstr ""

#: ../../netdicom-qr.rst:201
msgid "``-tf`` and ``-tt`` used together will search from ``tf`` to ``tt``."
msgstr ""

#: ../../netdicom-qr.rst:203
msgid "For example, to search for CT from 12 noon to 3pm on 19th March 2018, using remote QR node database ID 2 and local store database ID 1:"
msgstr ""

#: ../../netdicom-qr.rst:214
msgid "Query filtering logic"
msgstr ""

#: ../../netdicom-qr.rst:217
msgid "Study level query response processing"
msgstr ""

#: ../../netdicom-qr.rst:219
msgid "First we query for each modality chosen in turn to get matching responses at study level."
msgstr ""

#: ../../netdicom-qr.rst:220
msgid "If the optional ``ModalitiesInStudy`` has been populated in the response, and if you have ticked ``Include SR only studies``, then any studies with anything other than just ``SR`` studies is removed from the response list."
msgstr ""

#: ../../netdicom-qr.rst:223
msgid "If any study description or station name filters have been added, and if the ``StudyDescription`` and/or ``StationName`` tags are returned by the remote server, the study response list is filtered accordingly."
msgstr ""

#: ../../netdicom-qr.rst:225
msgid "For the remaining study level responses, each series is queried."
msgstr ""

#: ../../netdicom-qr.rst:226
msgid "If ``ModalitiesInStudy`` was not returned, it is now built from the series level responses."
msgstr ""

#: ../../netdicom-qr.rst:227
msgid "If the remote server returned everything rather than just the modalities we asked for, the study level responses are now filtered against the modalities selected."
msgstr ""

#: ../../netdicom-qr.rst:231
msgid "Series level query processing"
msgstr ""

#: ../../netdicom-qr.rst:233
msgid "Another attempt is made to exclude or only-include if station name filters have been set"
msgstr ""

#: ../../netdicom-qr.rst:235
msgid "If **mammography** exams were requested, and a study has ``MG`` in:"
msgstr ""

#: ../../netdicom-qr.rst:237
msgid "If one of the series is of type ``SR``, an image level query is done to see if it is an RDSR. If it is, all the other series responses are deleted (i.e. when the move request/'retrieve' is sent only the RDSR is requested not the images."
msgstr ""

#: ../../netdicom-qr.rst:240
#: ../../netdicom-qr.rst:246
msgid "Otherwise the ``SR`` series responses are deleted and all the image series are requested."
msgstr ""

#: ../../netdicom-qr.rst:242
msgid "If **planar radiographic** exams were requested, and a study has ``DX`` or ``CR`` in:"
msgstr ""

#: ../../netdicom-qr.rst:244
msgid "Any ``SR`` series are checked at 'image' level to see if they are RDSRs. If they are, the other series level responses for that study are deleted."
msgstr ""

#: ../../netdicom-qr.rst:248
msgid "If **fluoroscopy** exams were requested, and a study has ``RF`` or ``XA`` in:"
msgstr ""

#: ../../netdicom-qr.rst:250
msgid "Any ``SR`` series are checked at 'image' level to see if they are RDSRs or ESRs (Enhanced Structured Reports - not currently used but will be in the future). Any other ``SR`` series responses are deleted."
msgstr ""

#: ../../netdicom-qr.rst:252
msgid "All non-``SR`` series responses are deleted."
msgstr ""

#: ../../netdicom-qr.rst:254
msgid "If **CT** exams were requested, and a study has ``CT`` in:"
msgstr ""

#: ../../netdicom-qr.rst:256
msgid "Any ``SR`` series are checked at 'image' level to see if they are RDSRs. If they are, all other SR and image series responses are deleted. Otherwise, if it has an ESR series, again all other SR and image series responses are deleted."
msgstr ""

#: ../../netdicom-qr.rst:258
msgid "If there are no RDSR or ESR series, the other series are checked to see if they are Philips 'Dose info' series. If there are, other series responses are deleted."
msgstr ""

#: ../../netdicom-qr.rst:260
msgid "If there are no RDSR, ESR or 'Dose info' series and the option to get Toshiba images has been selected, then an image level query is performed for the first image in each series. If the image is not a secondary capture, all but the first image are deleted from the image level responses and the image_level_move flag is set. If the image is a secondary capture, the whole series response is kept."
msgstr ""

#: ../../netdicom-qr.rst:264
msgid "If there are no RDSR or ESR, series descriptions aren't returned and the Toshiba option has been set, the image level query is performed as per the previous point. This process will keep the responses that might have Philips 'Dose info' series."
msgstr ""

#: ../../netdicom-qr.rst:267
msgid "If there are no RDSR, ESR, series descriptions aren't returned and the Toshiba option has not been set, each series with more than five images in is deleted from the series response list - the remaining ones might be Philips 'Dose info' series."
msgstr ""

#: ../../netdicom-qr.rst:271
msgid "If **SR only studies** were requested:"
msgstr ""

#: ../../netdicom-qr.rst:273
msgid "Each series response is checked at 'image' level to see which type of SR it is. If is not RDSR or ESR, the study response is deleted."
msgstr ""

#: ../../netdicom-qr.rst:276
msgid "If **Get SR series that return nothing at image level query** were requested:"
msgstr ""

#: ../../netdicom-qr.rst:278
msgid "It is assumed that any ``SR`` series that appears to be empty actually contains an RDSR, and the other series are dealt with as above for when an RDSR is found. If at the image level query the full data requested is returned, then the series will be processed the same whether this option is selected or not."
msgstr ""

#: ../../netdicom-qr.rst:283
msgid "Duplicates processing"
msgstr ""

#: ../../netdicom-qr.rst:285
msgid "For each remaining study in the query response, the Study Instance UID is checked against the studies already in the OpenREM database."
msgstr ""

#: ../../netdicom-qr.rst:288
msgid "If there is a match and the series level modality is **SR** (from a CT, or RF etc):"
msgstr ""

#: ../../netdicom-qr.rst:290
msgid "The image level response will have the SOP Instance UID - this is checked against the SOP Instance UIDs recorded with the matching study. If a match is found, the 'image' level response is deleted."
msgstr ""

#: ../../netdicom-qr.rst:293
msgid "If there is a match and the series level modality is **MG**, **DX** or **CR**:"
msgstr ""

#: ../../netdicom-qr.rst:295
msgid "An image level query is made which will populate the image level responses with SOP Instance UIDs"
msgstr ""

#: ../../netdicom-qr.rst:296
msgid "Each image level response is then processed and the SOP Instance UID is checked against the SOP Instance UIDs recorded with the matching study. If a match is found, the 'image' level response is deleted."
msgstr ""

#: ../../netdicom-qr.rst:299
msgid "Once each series level response is processed:"
msgstr ""

#: ../../netdicom-qr.rst:301
msgid "If the series no longer has any image  level responses the series level response is deleted."
msgstr ""

#: ../../netdicom-qr.rst:302
msgid "If the study no longer has any series level responses the study level response is deleted."
msgstr ""

#: ../../netdicom-qr.rst:309
msgid "Troubleshooting: openrem_qr.log"
msgstr ""

#: ../../netdicom-qr.rst:311
msgid "If the default logging settings haven't been changed then there will be a log files to refer to. The default location is within your ``MEDIAROOT`` folder:"
msgstr ""

#: ../../netdicom-qr.rst:314
msgid "This file contains information about the query, the status of the remote node, the C-Find response, the analysis of the response, and the individual C-Move requests."
msgstr ""

#: ../../netdicom-qr.rst:317
msgid "The following is an example of the start of the log for the following query which is run once an hour (ie some responses will already have been imported):"
msgstr ""

#: ../../netdicom-qr.rst:392
msgid "If you are using an OpenREM native storage node, then you might also like to review :ref:`storetroubleshooting`"
msgstr ""
