# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-25 21:31+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../nginx_on_linux.rst:3
#: 2612bc5049334351b6388822c850d22b
msgid "Running OpenREM on Linux with Gunicorn and NGINX"
msgstr ""

#: ../../nginx_on_linux.rst:5
#: 25d05cdc4fb14c49b3939425477551ff
msgid "These instructions are for running OpenREM with Gunicorn and NGINX on Ubuntu Server, but should work on other Linux distributions with minimal modification. These instructions are based on a guide at `obeythetestingoat.com <https://www.obeythetestinggoat.com/book/chapter_making_deployment_production_ready.html>`_"
msgstr ""

#: ../../nginx_on_linux.rst:13
#: 156d4e92516949e6a1db0e6a4e98fc5f
msgid "Prerequisites"
msgstr ""

#: ../../nginx_on_linux.rst:15
#: 56ba0dfcccc5442b8052061db340def7
msgid "A working OpenREM installation, that serves web pages using the built-in web server. You can test this using the instructions in :doc:`startservices`"
msgstr ""

#: ../../nginx_on_linux.rst:18
#: ca47a74491104098b7eaadbacfd8cdc3
msgid "Contents"
msgstr ""

#: ../../nginx_on_linux.rst:21
#: df773eecb0564b0987faf36cc526210f
msgid "Steps to perform"
msgstr ""

#: ../../nginx_on_linux.rst:25
#: 545f21b1d5f1448f8de16830e723687b
msgid "If you get stuck somewhere in these instructions, please check the :ref:`troubleshooting` section at the end of this page."
msgstr ""

#: ../../nginx_on_linux.rst:29
#: a453d893fc0a41e8886b3ffb282d6c9f
msgid "Install NGINX"
msgstr ""

#: ../../nginx_on_linux.rst:36
#: ec0847f3140c4fbc925c73ccfb904a1e
msgid "You should now be able to see the 'Welcome to nginx' page if you go to your server address in a web browser."
msgstr ""

#: ../../nginx_on_linux.rst:39
#: 18d2db50cdd14a8fab7d625e5ded8470
msgid "Create initial nginx configuration"
msgstr ""

#: ../../nginx_on_linux.rst:41
#: a69c552b8a9b44eca84b9821a7cb92bd
msgid "Create a new config file - you can name the file as you like, but it is usually has the server name."
msgstr ""

#: ../../nginx_on_linux.rst:47
#: 2b504fe07a294090bd4c22f1a4901531
msgid "Start with the following settings - replace the server name with the hostname of your server. For this example, I will use ``openrem-server``"
msgstr ""

#: ../../nginx_on_linux.rst:61
#: 942fc3d187234390ac25ff98647d5c3f
msgid "Save and exit (see :ref:`nano` below for tips)."
msgstr ""

#: ../../nginx_on_linux.rst:63
#: 3e947fb80d184c3693091ef6cd483cc7
msgid "Now delete the default nginx configuration from ``sites-enabled`` and make a link to our new one:"
msgstr ""

#: ../../nginx_on_linux.rst:70
#: 16a48fda4da844e5aee9be252bc0e0fc
msgid "Now we reload nginx and start our server as before to test this step. At this stage, nginx is simply passing requests to the default port (80) on to port 8000 to be dealt with (which is why we need to start the test server again)."
msgstr ""

#: ../../nginx_on_linux.rst:80
#: 56ff446bbed84bd896d6b6817684e106
msgid "Now use your web browser to look at your server again - the 'Welcome to nginx' page should be replaced by an ugly version of the OpenREM website - this is because the 'static' files are not yet being served - we'll fix this later."
msgstr ""

#: ../../nginx_on_linux.rst:84
#: a874f909bd5441b7a254874367d5695a
msgid "Replace runserver with Gunicorn"
msgstr ""

#: ../../nginx_on_linux.rst:86
#: 868cd00c4ce24d0799b7f94346aba10c
msgid "Activate your virtualenv if you are using one (add sudo if your aren't), and:"
msgstr ""

#: ../../nginx_on_linux.rst:92
#: 003af390087249afa392b331b55568e2
msgid "Make sure you have stopped the test webserver (``Ctrl-c`` in the shell ``runserver`` is running in), then from the same openrem folder:"
msgstr ""

#: ../../nginx_on_linux.rst:99
#: 4000e31af8d64b02b6ee17188791d9d7
msgid "The Gunicorn server should start, and you should be able to see the same broken version of the web interface again."
msgstr ""

#: ../../nginx_on_linux.rst:102
#: 70e6da377ab04bd7975629f84e4ef014
msgid "Serve static files using nginx"
msgstr ""

#: ../../nginx_on_linux.rst:104
#: 4b8d1da42fad4cbeb629f99e98492f41
msgid "Create a folder called ``static`` somewhere that your webserver user will be able to get to - for example alongside the ``media`` folder, and set the permissions. So if you created your media folder in ``/var/openrem/media``, you might do this:"
msgstr ""

#: ../../nginx_on_linux.rst:114
#: 7b85bd94d7b84cc0b03f69b81b399f0c
msgid "Now edit your ``openremproject/local_settings.py`` config file to put the same path in the ``STATIC_ROOT``:"
msgstr ""

#: ../../nginx_on_linux.rst:123
#: 68704605356a43b5ad444c222d2bc862
msgid "Now use the Django ``manage.py`` application to pull all the static files into the new folder:"
msgstr ""

#: ../../nginx_on_linux.rst:129
#: 55391ef96cba4ccaa4fdafb9619a8808
msgid "Now we need to tell nginx to serve them:"
msgstr ""

#: ../../nginx_on_linux.rst:135
#: 9461ed4b36e44ae784196daa3e2bc4a6
msgid "And modify the file to add the ``static`` section - remember to put the path you have used instead of ``/var/openrem/static``"
msgstr ""

#: ../../nginx_on_linux.rst:153
#: 9424ad49f6c94372baf1701f54ca8d9b
msgid "Now reload nginx and gunicorn to see if it is all working..."
msgstr ""

#: ../../nginx_on_linux.rst:162
#: 77a68645d71147b0a7e02e41d5036780
msgid "Take another look, and it should all be looking nice now!"
msgstr ""

#: ../../nginx_on_linux.rst:165
#: d2677983ab364501ac36edc781331e49
msgid "Switch to using Unix Sockets"
msgstr ""

#: ../../nginx_on_linux.rst:167
#: e61f39a2f805420fafb92604b6ef333d
msgid "This step is optional, but does allow you more flexibility if you need to do anything else on this server using port 8000 as this installation of OpenREM will no longer be using that port. Instead we'll use 'sockets', which are like files on the disk. We put these in ``/tmp/``."
msgstr ""

#: ../../nginx_on_linux.rst:171
#: 2130e4eeecba4dabb263b2078043b6a6
msgid "Change the nginx configuration again (``sudo nano /etc/nginx/sites-available/openrem-server``):"
msgstr ""

#: ../../nginx_on_linux.rst:188
#: 15b00555b59f49bba35e1f4c3591bf14
msgid "Now restart Gunicorn, this time telling it to use the socket, after reloading nginx:"
msgstr ""

#: ../../nginx_on_linux.rst:196
#: 1a2d69b023df45baa7741806d148cfc3
msgid "The ``\\`` just allows the command to spread to two lines - feel free to put it all on one line."
msgstr ""

#: ../../nginx_on_linux.rst:198
#: 7ff825eeb4984bd5a8cdaf4897910f23
msgid "Check the web interface again, hopefully it should still be working!"
msgstr ""

#: ../../nginx_on_linux.rst:201
#: e71b11abb0ec4df4bcb7dd25cd1ab824
msgid "Start Gunicorn automatically"
msgstr ""

#: ../../nginx_on_linux.rst:203
#: cffaac7b59594ce78db7ce2b703afb00
msgid "We can use systemd on Ubuntu to ensure Gunicorn starts on boot and restarts if it crashes. As before, change each instance of ``openrem-server`` for the name of your server. You will need to change the ``WorkingDirectory`` to match the path to your openrem folder."
msgstr ""

#: ../../nginx_on_linux.rst:207
#: 92cdd1a09bba43a68379176bdf2ede85
msgid "For the gunicorn command, you will need to provide the full path to gunicorn, whether that is in ``/usr/local/bin/gunicorn`` or the bin folder of your virtualenv."
msgstr ""

#: ../../nginx_on_linux.rst:232
#: a7615779a8c446e597ebf170e50d4d9b
msgid "Make sure you have customised the ``WorkingDirectory`` path, the path to gunicorn, and the name of the socket file. The ``User`` would normally be ``www-data``."
msgstr ""

#: ../../nginx_on_linux.rst:237
#: 4fad9c38003c47bb96762159007370a8
msgid "If the user you have configured can't write to the ``STATIC_ROOT`` folder, the ``MEDIA_ROOT`` folder and the location the logs are configured to be written (usually in ``MEDIA_ROOT``), the systemd gunicorn service is likely to fail when started."
msgstr ""

#: ../../nginx_on_linux.rst:242
#: 46398447405645ac872c13ebad936a2b
msgid "Now enable the new configuration:"
msgstr ""

#: ../../nginx_on_linux.rst:253
#: 1efdf7e40f68406b9e5dbc63821b3697
msgid "You might like to see if it worked..."
msgstr ""

#: ../../nginx_on_linux.rst:259
#: 69b286b3baa74caea4760f656622d966
msgid "Look for ``Active: active (running)``"
msgstr ""

#: ../../nginx_on_linux.rst:263
#: 25aebf4e6c2a43779fcfbae342433cab
msgid "Making use of ALLOWED_HOSTS in local_settings.py"
msgstr ""

#: ../../nginx_on_linux.rst:265
#: 716cfd8725fd42b7a6600216cec64240
msgid "The default setting of ``ALLOWED_HOSTS`` is ``*`` which isn't secure, but is convenient! We should really change this to match the hostname of the server."
msgstr ""

#: ../../nginx_on_linux.rst:268
#: 9d5e24b3b07b43ff9014d00e66de787c
msgid "If your hostname is ``openrem-server``, and the fully qualified domain name is ``openrem-server.ad.hospital.org`` and IP address is ``10.212.18.209``, then you might configure ``ALLOWED_HOSTS`` in ``openremproject/local_settings.py`` to:"
msgstr ""

#: ../../nginx_on_linux.rst:281
#: abb495b66add43b7ab5703a710b7b605
msgid "Which hostnames do I need to put in ``ALLOWED_HOSTS``? You need to put in any hostnames you want people to be able to access your OpenREM web interface at. So if in your hospital you only type in the address bar the hostname (``http://openrem-server`` in this example), then that is all you need to add. If you only use the IP address, then add that. If you can use any of them, add them all :-)"
msgstr ""

#: ../../nginx_on_linux.rst:286
#: 2289451c0e954d15805210f414a0956d
msgid "Next we need to edit the nginx configuration again to make sure Django can see the hostname by adding the ``proxy_set_header`` configuration (else it gets lost before Django can check it):"
msgstr ""

#: ../../nginx_on_linux.rst:309
#: 4c74536f4ab844aaba8a3e86a52bb229
msgid "Now reload the nginx configuration and reload Gunicorn:"
msgstr ""

#: ../../nginx_on_linux.rst:316
#: bc7e4278a59c42cd80cf7a5266fde163
msgid "And check the web interface again. If it doesn't work due to the ``ALLOWED_HOSTS`` setting, you will get a 'Bad request 400' error."
msgstr ""

#: ../../nginx_on_linux.rst:320
#: adf2014e272442cb9cca03880ee85534
msgid "Increasing the timeout"
msgstr ""

#: ../../nginx_on_linux.rst:322
#: 5eefdc1d81554c58bc6a99d68efd21df
msgid "You may wish to do this to allow for :doc:`skindosemap` that can take more than 30 seconds for complex studies. Both Gunicorn and nginx configurations need to be modified:"
msgstr ""

#: ../../nginx_on_linux.rst:329
#: 00bb9bcfc0944713a2c0dd312460ce9a
msgid "Add the ``--timeout`` setting to the end of the ``ExecStart`` command, time is in seconds (300 = 5 minutes, 1200 = 20 minutes)"
msgstr ""

#: ../../nginx_on_linux.rst:354
#: 1804cdbbaefe4c8f914e475948c4941a
msgid "Add the ``proxy_read_timeout`` setting in seconds (note the trailing ``s`` this time)."
msgstr ""

#: ../../nginx_on_linux.rst:373
#: e34fe1ce7070491ab56e4a11493f814b
msgid "Reload everything:"
msgstr ""

#: ../../nginx_on_linux.rst:383
#: dbdece81094f406fb6d41ab11bf8937a
msgid "If you have jumped straight to here to get the final config, then make sure you substitute all the following values to suit your install:"
msgstr ""

#: ../../nginx_on_linux.rst:386
#: 0edc40be047a44ba964ce1f4dcb824b8
msgid "``gunicorn-openrem.service`` - name not important (except the ``.service``, but you need to use it in the reload commands etc"
msgstr ""

#: ../../nginx_on_linux.rst:388
#: 90ebe35e280b4ea7b1e534e41e3d28fc
msgid "``User=www-data`` as appropriate. This should either be your user or ``www-data``. You will need to ensure folder permissions correspond"
msgstr ""

#: ../../nginx_on_linux.rst:390
#: 71585b1c02254fcb8f03e80c8259cdcf
msgid "``WorkingDirectory`` needs to match the path to your ``openrem`` folder (the one with ``manage.py`` in)"
msgstr ""

#: ../../nginx_on_linux.rst:391
#: 239688d9a04f42509bb3f62c09d7397a
msgid "``ExecStart=/usr/local/bin/gunicorn \\`` needs to match the path to your ``gunicorn`` executable - either in your virtualenv bin folder or system wide as per the example"
msgstr ""

#: ../../nginx_on_linux.rst:393
#: 9220501c276b446db37afdccb5594fe2
msgid "``--bind unix:/tmp/openrem-server.socket \\`` name in ``tmp`` doesn't matter, needs to match in gunicorn and nginx configs"
msgstr ""

#: ../../nginx_on_linux.rst:395
#: e97f01287d754d37b5ebb5b9955b3658
msgid "``/etc/nginx/sites-available/openrem-server`` ie name of config file in nginx, doesn't matter, usually matches hostname"
msgstr ""

#: ../../nginx_on_linux.rst:397
#: 5317cf3771854da390d5a001ec914d44
msgid "``server_name openrem-server`` - should match hostname"
msgstr ""

#: ../../nginx_on_linux.rst:398
#: 0a253fa42396496b88b6a8c21a5277bc
msgid "``/var/openrem/static`` folder must exist, with the right permissions. Location not important, must match setting in ``local_settings``"
msgstr ""

#: ../../nginx_on_linux.rst:400
#: d2aadf670b2040fb9da9f8b89b11b74a
msgid "``proxy_pass http://unix:/tmp/openrem-server.socket;`` must match setting in gunicorn config, prefixed with ``http://``"
msgstr ""

#: ../../nginx_on_linux.rst:403
#: f1bbe4dc85134b32acbe315d8b1ba4d2
msgid "You will also need to ``collectstatic``, symlink the nginx configuration into enabled, enable the gunicorn systemd config to start on reboot, and you should configure the ``ALLOWED_HOST`` setting. And you will need to have installed nginx and gunicorn!"
msgstr ""

#: ../../nginx_on_linux.rst:411
#: c7888c8206c24f2399430b9524e8e847
msgid "Troubleshooting and tips"
msgstr ""

#: ../../nginx_on_linux.rst:414
#: 76d4f96d6070423196af7de3e31f421d
msgid "less"
msgstr ""

#: ../../nginx_on_linux.rst:415
#: 583300a3e5cc4aa581bbeb213b668d02
msgid "Use ``less`` to review files without editing them"
msgstr ""

#: ../../nginx_on_linux.rst:417
#: 7ebeec8113c0425992bb3a4eb98910c3
msgid "Navigate using arrow keys, page up and down,"
msgstr ""

#: ../../nginx_on_linux.rst:418
#: 6457f354a9d048caa5167fdcfa7e8cce
msgid "``Shift-G`` to go to the end"
msgstr ""

#: ../../nginx_on_linux.rst:419
#: 79b08570620e47deabab44f93e138d02
msgid "``Shift-F`` to automatically update as new logs are added. ``Ctrl-C`` to stop."
msgstr ""

#: ../../nginx_on_linux.rst:420
#: 8c580bdc46334f5fa6a0846b43867221
msgid "``/`` to search"
msgstr ""

#: ../../nginx_on_linux.rst:425
#: 5a5608cb7ed54ab3b30a8af8ece9dde4
msgid "nano"
msgstr ""

#: ../../nginx_on_linux.rst:426
#: 63fbe73582884c2fae66e29b64d7b99e
msgid "Use ``nano`` to edit the files."
msgstr ""

#: ../../nginx_on_linux.rst:428
#: 1efbc4a8c75c4d31bfb93f4a8d31700b
msgid "``Ctrl-o`` to save ('out')"
msgstr ""

#: ../../nginx_on_linux.rst:429
#: 195cf760ed2841f9a33fec8531512b92
msgid "``Ctrl-x`` to exit"
msgstr ""

#: ../../nginx_on_linux.rst:432
#: e74dfa7bb9b7428fb5d5aebe074d51cf
msgid "Nginx"
msgstr ""

#: ../../nginx_on_linux.rst:434
#: 20ae6aca64604b669c93c14546546036
msgid "Logs are located in ``/var/log/nginx/``"
msgstr ""

#: ../../nginx_on_linux.rst:435
#: 8d3c5804231f497d8208cbf409084f84
msgid "You need root privileges to view the files:"
msgstr ""

#: ../../nginx_on_linux.rst:437
#: 328eb8e228604a6d9b540052f5ab06e1
msgid "To view latest error log: ``sudo less /var/log/nginx/error.log``"
msgstr ""

#: ../../nginx_on_linux.rst:439
#: 6ffc5151b90e47169479b318bdafc6a8
msgid "Reload: ``sudo systemctl reload nginx``"
msgstr ""

#: ../../nginx_on_linux.rst:440
#: 4acd3a2be7fd44f28f05926711790593
msgid "Check nginx config: ``sudo nginx -t``"
msgstr ""

#: ../../nginx_on_linux.rst:443
#: 75befa935c264661a1fa405794ff8216
msgid "Systemd and Gunicorn"
msgstr ""

#: ../../nginx_on_linux.rst:445
#: 8cd3892df7a943dc8faedf84ad1a5efa
msgid "Review the logs with ``sudo journalctl -u gunicorn-openrem`` (change as appropriate for the the name you have used)"
msgstr ""

#: ../../nginx_on_linux.rst:447
#: eab4e069b7954e58be6267052c6474d8
msgid "Check the systemd configuration with ``systemd-analyze verify /etc/systemd/system/gunicorn-openrem.service`` - again changing the name as appropriate."
msgstr ""

#: ../../nginx_on_linux.rst:449
#: c9478f41398348a7a38ffd2fe71beb80
msgid "If you make changes, you need to use ``sudo systemctl daemon-reload`` before the changes will take effect."
msgstr ""

#: ../../nginx_on_linux.rst:450
#: 083747ef6d23473f9a6cf11fd907c1a2
msgid "Restart: ``sudo systemctl restart gunicorn-openrem.service``"
msgstr ""
