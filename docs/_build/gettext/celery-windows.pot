# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../celery-windows.rst:3
msgid "Daemonising Celery and Flower on Windows"
msgstr ""

#: ../../celery-windows.rst:5
msgid "To ensure that the Celery task queue and Flower are started at system start-up it is advisable to launch them using batch files and configure Windows Task Scheduler to run each of these at system start-up."
msgstr ""

#: ../../celery-windows.rst:9
msgid "Celery will sometimes fall over during the execution of a long task. If Celery frequently falls over on your system then Windows Task Scheduler can be used to restart Celery on a regular basis. The Task Scheduler can also be used to ensure celery is running a few minutes prior to scheduled PACS queries."
msgstr ""

#: ../../celery-windows.rst:14
msgid "An example batch file is shown below for running and restarting Celery. This calls separate batch files to start Celery and Flower, also shown below."
msgstr ""

#: ../../celery-windows.rst:18
msgid "Celery control batch file"
msgstr ""

#: ../../celery-windows.rst:20
msgid "`celery_task.bat`, to be run as a scheduled task."
msgstr ""

#: ../../celery-windows.rst:47
msgid "Celery start batch file"
msgstr ""

#: ../../celery-windows.rst:49
msgid "`celery_start.bat`, called by `celery_task.bat`."
msgstr ""

#: ../../celery-windows.rst:70
msgid "Flower start batch file"
msgstr ""

#: ../../celery-windows.rst:72
msgid "`flower_start.bat`, called by `celery_task.bat` and also used to start Flower at system start-up."
msgstr ""

#: ../../celery-windows.rst:93
msgid "Setting up a scheduled task"
msgstr ""

#: ../../celery-windows.rst:96
msgid "For Celery"
msgstr ""

#: ../../celery-windows.rst:98
msgid "Open ``Task Scheduler`` on the OpenREM server and then click on the ``Task Scheduler Library`` item in the left-hand pane. This should look something like figure 1 below, but without the OpenREM tasks present."
msgstr ""

#: ../../celery-windows.rst:108
msgid "Figure 1: An overview of Windows Task Scheduler"
msgstr ""

#: ../../celery-windows.rst:110
msgid "To create a new task for celery click on ``Create Task...`` in the ``Actions`` menu in the right-hand pane. Give the task a name and description. Next, click on the ``Change User or Group`` button and type ``system`` in to the box, then click ``Check Names``, then click ``OK``. This sets the server's ``SYSTEM`` user to run the task. Also check the ``Run with highest prilileges`` box. Your task should now look similar to figure 2."
msgstr ""

#: ../../celery-windows.rst:123
msgid "Figure 2: General properties"
msgstr ""

#: ../../celery-windows.rst:125
msgid "Next, click on the ``Triggers`` tab so that you can set when the task will be run. As a minimum you should add an ``At startup`` trigger. To do this, click ``New...``. In the dialogue box that appears select ``At startup`` from the ``Begin the task`` options and ensure that the ``Enabled`` checkbox is selected. Then click ``OK``. You may wish to add other triggers that take place at specific times during the day, as shown in figure 3."
msgstr ""

#: ../../celery-windows.rst:131
msgid "In the example shown in figure 3 celery is started at system start up, and restarted multiple times each day to ensure that it is running before any PACS queries. Your requirements may be more straightforward than this example."
msgstr ""

#: ../../celery-windows.rst:141
msgid "Figure 3: Trigger properties"
msgstr ""

#: ../../celery-windows.rst:143
msgid "Now click on the ``Actions`` tab so that you can add the action that is taken when the task is run. Click on ``New...``, and in the dialogue box that appears select ``Start a program`` as the ``Action``. Click on ``Browse`` and select the celery batch file that you created earlier. Click ``OK`` to close the ``New Action`` dialogue box. Figure 4 shows an example of the the ``Actions`` tab."
msgstr ""

#: ../../celery-windows.rst:155
msgid "Figure 4: Action properties"
msgstr ""

#: ../../celery-windows.rst:158
msgid "There are no particular conditions set for the task, as shown in figure 5."
msgstr ""

#: ../../celery-windows.rst:166
msgid "Figure 5: Condition properties"
msgstr ""

#: ../../celery-windows.rst:169
msgid "Finally, click on the ``Settings`` tab (figure 6). Check the ``Allow task to be run on demand`` box, and also the ``If the running task does not end when requested, force it to stop`` box. Choose ``Stop the existing instance`` from the ``If the task is already running, then the following rule applies:`` list. Then click the ``OK`` button to add the task to the scheduler library."
msgstr ""

#: ../../celery-windows.rst:180
msgid "Figure 6: Task settings"
msgstr ""

#: ../../celery-windows.rst:184
msgid "For Flower"
msgstr ""

#: ../../celery-windows.rst:186
msgid "Repeat the above steps for the Flower batch file, but only configure the Flower task to trigger on system start-up: there should be no need to schedule re-starts of Flower."
msgstr ""
