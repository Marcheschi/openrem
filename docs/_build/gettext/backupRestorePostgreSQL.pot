# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, The Royal Marsden NHS Foundation Trust
# This file is distributed under the same license as the OpenREM package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: OpenREM 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 18:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../backupRestorePostgreSQL.rst:2
msgid "Backing up a PostgreSQL database (Windows)"
msgstr ""

#: ../../backupRestorePostgreSQL.rst:4
msgid "Content contributed by DJ Platten"
msgstr ""

#: ../../backupRestorePostgreSQL.rst:6
msgid "These instructions are based on PostgreSQL 9.1 and OpenREM 0.5.0 running on Windows Server 2008. The database restore has been tested on Ubuntu 12.04 LTS."
msgstr ""

#: ../../backupRestorePostgreSQL.rst:9
msgid "Create a PostgreSQL user called ``backup`` with a password of ``backup``. This is easiest to do using the ``pgAdminIII`` tool: you'll need to create a new login role. In the role privileges ensure that at least ``Can login``, ``Superuser`` and ``Can initiate streaming replication and backups`` are checked."
msgstr ""

#: ../../backupRestorePostgreSQL.rst:12
msgid "The ``pgAdminIII`` tool is available by default on Windows, but needs to be explicitly installed if using Ubuntu with the following command:"
msgstr ""

#: ../../backupRestorePostgreSQL.rst:18
msgid "For the remainder of this article I'm going to assume that your OpenREM database is called ``openrempostgresql``."
msgstr ""

#: ../../backupRestorePostgreSQL.rst:20
msgid "To backup the contents of ``openrempostgresql`` to a file called ``backup.sql`` run the following at the command line in a command prompt (Windows), or terminal window (Ubuntu):"
msgstr ""

#: ../../backupRestorePostgreSQL.rst:26
msgid "You will need to add your ``C:\\path\\to\\postgres\\bin`` folder to the ``path`` environment variable for this to work. Make sure to use the actual path to your PostgreSQL ``bin`` folder rather than the example text provided here. See http://www.computerhope.com/issues/ch000549.htm for instructions on editing the path environment variable."
msgstr ""

#: ../../backupRestorePostgreSQL.rst:30
msgid "The ``-U backup`` indicates that the ``backup`` user is to carry out the task. The ``-F c`` option archives in a suitable format for input into the ``pg_restore`` command. Further information on ``pg_dump`` and backing up a PostgreSQL database can be found here: http://www.postgresql.org/docs/9.3/static/app-pgdump.html and here: http://www.postgresql.org/docs/9.3/static/backup-dump.html"
msgstr ""

#: ../../backupRestorePostgreSQL.rst:33
msgid "Restoring a PostgreSQL database (Windows)"
msgstr ""

#: ../../backupRestorePostgreSQL.rst:35
msgid "The ``pg_restore`` command can be used to restore the database using one of the backed-up SQL files that were produced using the ``pg_dump`` command."
msgstr ""

#: ../../backupRestorePostgreSQL.rst:37
msgid "Use the ``pgAdminIII`` tool to ensure that there is a PostgreSQL user called ``openremuser``."
msgstr ""

#: ../../backupRestorePostgreSQL.rst:39
msgid "Use ``pgAdminIII`` to create a database called ``openrempostgresql``; set the owner to ``openremuser`` and the encoding to ``UTF8``."
msgstr ""

#: ../../backupRestorePostgreSQL.rst:41
msgid "Run the following command in a command prompt window (Windows) or terminal window (Ubuntu) to restore the contents of ``backupFile`` to the ``openrempostgresql`` database, where ``backupFile`` is the file created by the ``pg_dump`` command:"
msgstr ""

#: ../../backupRestorePostgreSQL.rst:47
msgid "Ensure that ``openremuser`` has an entry in PostgreSQL’s ``pg_hpa.conf`` file for md5 authentication:"
msgstr ""

#: ../../backupRestorePostgreSQL.rst:53
msgid "The PostgreSQL server will need to be restarted if you have changed ``pg_hpa.conf``."
msgstr ""

#: ../../backupRestorePostgreSQL.rst:55
msgid "See http://www.postgresql.org/docs/9.3/static/backup-dump.html#BACKUP-DUMP-RESTORE for further details."
msgstr ""
